package com.tarjetamiga.view;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.Convenio;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.common.AbstractView;
import com.tarjetamiga.enums.FormatoArchivosENUM;
import com.tarjetamiga.service.ISolicitudFacturaBS;

@ManagedBean
@ViewScoped
public class FacturaNuevaView extends AbstractView implements Serializable{
	
	static final Logger logger = LogManager.getLogger(FacturaNuevaView.class.getName());

	private SolicitudFactura objSolicitudFactura;
	private Collection<Convenio> lstConvenio;
	
	private Concepto objConcepto;
	private Collection<Concepto> lstConcepto;
	private Collection<Archivo> lstArchivos;
	
	private Archivo objArchivoSeleccionado;
	
	private String mensaje;//Mensaje de error
	
	@ManagedProperty(value="#{solicitudFacturaBS}")//Nombre de la clase SolicitudFacturaBS.java
	private ISolicitudFacturaBS objSolicitudFacturaBS;
	
	@PostConstruct
	public void inicializar(){
		
		setLstArchivos(new ArrayList<Archivo>());
		
		setObjSolicitudFactura(new SolicitudFactura());
		setLstConvenio(new ArrayList<Convenio>());
		
		setObjConcepto(new Concepto());
		setLstConcepto(new ArrayList<Concepto>());
		
		//Inicia llenado de lista de convenios.
				Convenio objConvenio = new Convenio();
				objConvenio.setId(1);
				objConvenio.setDescripcion("Seven Eleven");
				
				getLstConvenio().add(objConvenio);
				
				objConvenio = new Convenio();
				objConvenio.setId(2);
				objConvenio.setDescripcion("Oxxo");
				
				getLstConvenio().add(objConvenio);
				
				objConvenio = new Convenio();
				objConvenio.setId(3);
				objConvenio.setDescripcion("Soriana");
				
				getLstConvenio().add(objConvenio);
	  //Termina llenado de lista de convenios.
		
	}
	
	public String guardar(){
		
		String pagina = "";
		
		if(validaSolicitud() == true)
		{
			
			Format formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String fechaString = formatter.format(new  Date());
			
			getObjSolicitudFactura().setFechaCreacion(fechaString);
			
			getObjSolicitudFactura().setLstConceptos((List<Concepto>) getLstConcepto());
			
			try {
				int codigoError = getObjSolicitudFacturaBS().crearSolicitud(getObjSolicitudFactura(), 
						resourceBundlePropiedades("correoOrigen"), 
						resourceBundlePropiedades("correoDestino") ,lstArchivos);
				
				if(codigoError == 0)
				{
					pagina = "busquedaSolicitudFactura.xhtml?faces-redirect=true";
					
					FacesContext context = FacesContext.getCurrentInstance();
					
					Flash flash = context.getExternalContext().getFlash();
					flash.setKeepMessages(true);
					flash.setRedirect(true);
					
					context.addMessage("nuevoMensaje", new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito: Se creo de forma correcta el registro: "+getObjSolicitudFactura().getDescripcion(),"") );
					
				}else{
					mensajes("Error al crear la solicitud.", FacesMessage.SEVERITY_ERROR);
				}
				
				
			}
			catch (Exception e) {
				
				logger.error("esto es un mensaje de tipo: error",e);
				
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Ocurrio un error, favor de intentar mas tarde.", "") );
			
			}
			
			
			
			
			
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("mensajeGrowl", new FacesMessage(FacesMessage.SEVERITY_WARN, "Precaucion:"+getMensaje(), "") );
			
		}
		
		return pagina;
	}
	
	public boolean validaSolicitud(){
		
		boolean booEsValido = true;
		
		if(getObjSolicitudFactura().getDescripcion().trim().equals(""))
		{
			setMensaje("Descripcion: Descripcion es requerido.");
			booEsValido = false;
		}else{
			if(getLstConcepto().size() == 0)
			{
				setMensaje("Conceptos: Debe de agregar por lo menos un concepto.");
				booEsValido = false;
			}
		}
		
		return booEsValido;
	}
	
	public void agregar(){
		getLstConcepto().add(getObjConcepto());
		setObjConcepto(new Concepto());
	}
	
	public void eliminar(Concepto objConcepto){
		getLstConcepto().remove(objConcepto);
	}
	
	
	
	public void subirArchivo(FileUploadEvent event) {
		
		UploadedFile file = event.getFile();
        
		if(lstArchivos != null && lstArchivos.size() == 0)
		{
			
			if(FormatoArchivosENUM.getTipoArchivo(file.getContentType()) != null)
			{
				if(file.getFileName() != null && file.getFileName().trim().length() <= 100)
				{
					Archivo objArchivo = new Archivo();
					objArchivo.setArrBytesArchivo(file.getContents());
					objArchivo.setNombreArchivo(file.getFileName());
					lstArchivos.add(objArchivo);
				}else{
					mensajes("El nombre del archivo excede los 100 caracteres incluyendo la extension (.pdf,.xlsx,etc.)", FacesMessage.SEVERITY_ERROR);
				}
			}else{
				mensajes("El tipo de archivo no es valido", FacesMessage.SEVERITY_ERROR);
			}
			
		}else{
			mensajes("Ya tiene un archivo cargado.", FacesMessage.SEVERITY_ERROR);
		}
		
    }
	
	
	public void eliminarArchivo(){

		Iterator<Archivo> lstArchivosCargados = lstArchivos.iterator();
		
		while (lstArchivosCargados.hasNext())
		{
			Archivo obj = lstArchivosCargados.next();
			
			if(obj.getNombreArchivo().equals(objArchivoSeleccionado.getNombreArchivo()))
			{
				lstArchivosCargados.remove();
				break;
			}
		}
		
		
		
//		
//		for(Archivo obj : lstArchivos)
//		{
//			if(objArchivoSeleccionado.getNombreArchivo())
//		}
//		

		
	}
	
	

	public SolicitudFactura getObjSolicitudFactura() {
		return objSolicitudFactura;
	}

	public void setObjSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		this.objSolicitudFactura = objSolicitudFactura;
	}

	public Collection<Concepto> getLstConcepto() {
		return lstConcepto;
	}

	public void setLstConcepto(Collection<Concepto> lstConcepto) {
		this.lstConcepto = lstConcepto;
	}

	public Concepto getObjConcepto() {
		return objConcepto;
	}

	public void setObjConcepto(Concepto objConcepto) {
		this.objConcepto = objConcepto;
	}


	public Collection<Convenio> getLstConvenio() {
		return lstConvenio;
	}


	public void setLstConvenio(Collection<Convenio> lstConvenio) {
		this.lstConvenio = lstConvenio;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ISolicitudFacturaBS getObjSolicitudFacturaBS() {
		return objSolicitudFacturaBS;
	}

	public void setObjSolicitudFacturaBS(ISolicitudFacturaBS objSolicitudFacturaBS) {
		this.objSolicitudFacturaBS = objSolicitudFacturaBS;
	}

	public Collection<Archivo> getLstArchivos() {
		return lstArchivos;
	}

	public void setLstArchivos(Collection<Archivo> lstArchivos) {
		this.lstArchivos = lstArchivos;
	}

	public Archivo getObjArchivoSeleccionado() {
		return objArchivoSeleccionado;
	}

	public void setObjArchivoSeleccionado(Archivo objArchivoSeleccionado) {
		this.objArchivoSeleccionado = objArchivoSeleccionado;
	}
	
	
}
