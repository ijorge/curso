package com.tarjetamiga.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.tarjetamiga.common.AbstractView;

@ManagedBean
@ViewScoped
public class GraficaVentaConsolaView  extends AbstractView implements Serializable{

	
	private LineChartModel areaModel;
	
	@PostConstruct
    public void init() {
        createAreaModel();
    }
 
	
	
	 private void createAreaModel() {
	        areaModel = new LineChartModel();
	        
	        LineChartSeries series1 = new LineChartSeries();
	        series1.setLabel("Wii");
	 
	        series1.set("2014-01", 2);
	        series1.set("2014-02", 7);
	        series1.set("2014-03", 1);
	        series1.set("2014-04", 3);
	        series1.set("2014-05", 8);
	 
	        LineChartSeries series2 = new LineChartSeries();
	        series2.setLabel("PS 3");
	 
	        series2.set("2014-01", 9);
	        series2.set("2014-02", 5);
	        series2.set("2014-03", 5);
	        series2.set("2014-04", 7);
	        series2.set("2014-05", 3);
	        
	        LineChartSeries series3 = new LineChartSeries();
	        series3.setLabel("Xbox 360");
	 
	        series3.set("2014-01", 4);
	        series3.set("2014-02", 2);
	        series3.set("2014-03", 7);
	        series3.set("2014-04", 2);
	        series3.set("2014-05", 1);
	 
	        areaModel.addSeries(series1);
	        areaModel.addSeries(series2);
	        areaModel.addSeries(series3);
	         
	        areaModel.setTitle("Ventas del año");//titulo de la grafica
	        areaModel.setLegendPosition("ne"); // Posicion de los conceptos: n s e w
	        areaModel.setStacked(false);//Si se establece a TRUE, suma/apila los valores.
	        areaModel.setShowPointLabels(true);
	         
	        Axis xAxis = new CategoryAxis();//Se crea objeto de ejes
	        xAxis.setLabel("Meses");//Se asigna titulo al eje
	        
	        areaModel.getAxes().put(AxisType.X, xAxis);//Se asigna el objeto eje al 'eje x' al modelo
	        
	        Axis yAxis = areaModel.getAxis(AxisType.Y);//Se obtiene el eje Y del modelo
	        yAxis.setLabel("Millones de consolas");//Se le asigna el titulo.
	        
	        yAxis.setMin(0);//Minimo del eje Y
	        yAxis.setMax(15);//Maximo del eje Y
	        
	        areaModel.setZoom(true);//Habilitamos el zoom
	        areaModel.setAnimate(true);//Animacion a la grafica
	    }
	

	public LineChartModel getAreaModel() {
		return areaModel;
	}

	public void setAreaModel(LineChartModel areaModel) {
		this.areaModel = areaModel;
	}
	
}
