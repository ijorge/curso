package com.tarjetamiga.view;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.service.ILoginBS;

@ManagedBean
@ViewScoped
public class LoginView {
	
	private static final String USUARIO = "usuario" ;
	private static final String MODEL = "model" ;
	private static final String PERMISOS_MENU = "permisosMenu" ;
	
	static final Logger logger = LogManager.getLogger(LoginView.class.getName());
	
	private Usuario objUsuario;
	private String mensaje;
	
	private MenuModel model;
	
	@ManagedProperty(value="#{loginBS}")
	private ILoginBS objLoginBS;
	
	@PostConstruct
	public void inicializa(){
		this.objUsuario = new Usuario();
		this.mensaje = "";
	}
	
	public String entrar(){
		String pagina = "";
		
//		for(int i = 0 ; i < 200; i++)
//		{
			
			
//			if (logger.isDebugEnabled())
//			{
				
//			}
			
			
			
			logger.fatal("esto es un mensaje de tipo: fatal");
			logger.error("esto es un mensaje de tipo: error");
			logger.warn("esto es un mensaje de tipo: warn");
			logger.info("esto es un mensaje de tipo: info");
			logger.debug("esto es un mensaje de tipo: debug");
			
//		}
		
		if(getObjLoginBS().existeUsuario(getObjUsuario()))
		{
			
			if(getObjLoginBS().validaUsuarioSP(getObjUsuario()))
			{
				//Llenamos objeto usuario.
				getObjUsuario().setApellidoPaterno("Arrambide");
				getObjUsuario().setApellidoMaterno("Hernandez");
				getObjUsuario().setNombre("Jorge");
				getObjUsuario().setCorreo("jorge@mail.com");
				
				FacesContext facesContext = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
				session.setAttribute(USUARIO, getObjUsuario());

				HashMap<Integer, ArrayList<PerfilMenu>> mapPerfilMenu = crearMenu();
				
				session.setAttribute(MODEL, this.model);
				session.setAttribute(PERMISOS_MENU, mapPerfilMenu);
				

				pagina = "inicio";
			}else{
				setMensaje("Usuario o Contraseña invalido.");
			}
			
			
			
		}else{
			setMensaje("Usuario o Contraseña invalido.");
		}
		
		return pagina;
	}
	
	/**
	 * Metodo para crear el menu
	 */
	public HashMap<Integer, ArrayList<PerfilMenu>> crearMenu(){
		
		//Se obtiene los permisos del usuario
		HashMap<Integer, ArrayList<PerfilMenu>> mapPerfilMenu = getObjLoginBS().obtenerMenu(getObjUsuario());
		
		//Se crea objeto Menu
		model = new DefaultMenuModel();

		//Se inicializa opcion
		DefaultSubMenu opcionPadre = null;	
		
		//Se crea el menu
		menu(mapPerfilMenu,0,opcionPadre);
		
		return mapPerfilMenu;
		
	}
	
	/**
	 * Metodo para obtener padre e hijos del menu.
	 * @param mapa
	 * @param padre
	 * @param opcionPadre
	 */
	public void menu(HashMap<Integer, ArrayList<PerfilMenu>> mapa, int padre, DefaultSubMenu opcionPadre){
		
		ArrayList<PerfilMenu> listaHijos = mapa.get(padre);
		
		if(listaHijos != null && listaHijos.size() > 0)
		{
			
			for(int i = 0; i < listaHijos.size() ; i++)
			{
				
				
				if(padre == 0)
				{//RAIZ
						
						//Se crea la opcion
						 opcionPadre = new DefaultSubMenu(listaHijos.get(i).getMenu().getDescripcion());
						 
						 //Se agrega al modelo
						 model.addElement(opcionPadre);
						 
						 //Llamamos al metodo menu para que revise sus hijos
						 menu(mapa, listaHijos.get(i).getMenu().getId(), opcionPadre);
					 
				}else{//No es RAIZ
					
					if(mapa.get(listaHijos.get(i).getMenu().getId()) != null && mapa.get(listaHijos.get(i).getMenu().getId()).size() > 0)
					{//Hijo que tiene hijos => es padre
						 
						//Se crea la opcion
						 DefaultSubMenu subPadre = new DefaultSubMenu(listaHijos.get(i).getMenu().getDescripcion());

						//Llamamos al metodo menu para que revise sus hijos
						 menu(mapa, listaHijos.get(i).getMenu().getId(), subPadre);
						 
						 //Agrega la opcion
						 opcionPadre.addElement(subPadre);
						 
						 
					}else{
						
							//Solo es hijo (este elemento ya no tiene hijos)
						
							//Se crea opcion
						 	DefaultMenuItem item = new DefaultMenuItem(listaHijos.get(i).getMenu().getDescripcion());
					        item.setCommand("#{menuView.clicOpcionMenu}");
					        item.setAjax(false);
					        item.setParam("url", listaHijos.get(i).getMenu().getUrl()+"?faces-redirect=true");
					        item.setParam("agregar", listaHijos.get(i).isAgregar());
					        item.setParam("modificar", listaHijos.get(i).isEditar());
					        item.setParam("consultar", listaHijos.get(i).isConsulta());
					        item.setParam("eliminar", listaHijos.get(i).isEliminar());
					        
					        //Se agrega al elemento padre
					        opcionPadre.addElement(item);
					        
					}
				}	
			}
		}

   

	}
	
	
	
	public String salir(){
		((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
		return "/login";
	}

	public Usuario getObjUsuario() {
		return objUsuario;
	}

	public void setObjUsuario(Usuario objUsuario) {
		this.objUsuario = objUsuario;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ILoginBS getObjLoginBS() {
		return objLoginBS;
	}

	public void setObjLoginBS(ILoginBS objLoginBS) {
		this.objLoginBS = objLoginBS;
	}

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

}
