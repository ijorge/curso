package com.tarjetamiga.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;


import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.common.AbstractView;
import com.tarjetamiga.common.nodo.Node;
import com.tarjetamiga.common.nodo.Tree;
import com.tarjetamiga.service.IPerfilBS;



@ManagedBean
@ViewScoped
public class PerfilAltaView extends AbstractView  implements Serializable{
	
	private static final long serialVersionUID = 3938052439101883381L;

	@ManagedProperty(value="#{perfilBS}")
	private IPerfilBS perfilBS;
	
	//Pantalla alta de perfil
	private Perfil objPerfil;
	private Tree<PerfilMenu> arbol = new Tree<PerfilMenu>();
	private Node<PerfilMenu> nodoRaiz = new Node<PerfilMenu>();
	private boolean editable = false;//Variable global para habilitar checkbox (false.- permite editar, true.- no permite editar)
	
	
	/**
	 * Metodo para regregar a la pantalla de busqueda.
	 * @return
	 */
	public String cancelar(){
				
		return "/adminPerfil/busquedaPerfil.xhtml?faces-redirect=true";
	}
	
	@PostConstruct
	public void init() {
		obtenerUsuario();
		inicializarCamposAlta();

	}
	
	/**
	 * Metodo que para crear el menu en forma de arbol.
	 * Usa recursividad, se llama asi mismo para ir a�adiendo a los hijos.
	 * @param arbol.- Parametro de tipo PerfilMenu
	 * @param nodo.- Nodode tipo PerfilMenu
	 * @param mapa.- Mapa que contiene el menu, en donde el key es el nodo padre, y el value el la lista de hijos.
	 * @param padre.- identificador del elemento padre.
	 */
	public void arbolMenu(Tree<PerfilMenu> arbol, Node<PerfilMenu> nodo , HashMap<Integer, ArrayList<PerfilMenu>> mapa, int padre){
		
		if(mapa != null && mapa.size() > 0)
		{
			
			ArrayList<PerfilMenu> listaHijos = mapa.get(padre);
			
			if(listaHijos != null && listaHijos.size() > 0)
			{
				
				for(PerfilMenu obj : listaHijos)
				{
					
					
					if(padre == 0)
					{//Padre raiz (Lo que va hasta arriba en el Menu.)
						Node<PerfilMenu> objNuevoNodo = new Node<PerfilMenu>();
						
						ArrayList<PerfilMenu> lista = mapa.get(obj.getMenu().getId());
						
						boolean tieneHijoSeleccionado = false;
						
						if(lista != null)
						{
							for(PerfilMenu objPM : lista)
							{
								if(objPM.isSeleccionado())
								{
									tieneHijoSeleccionado = true;
									break;
								}
							}
						}
						
						obj.setSeleccionado(tieneHijoSeleccionado);
						
						objNuevoNodo.setData(obj);
						
						arbol.getRootElement().addChild(objNuevoNodo);
						
						arbolMenu(arbol,objNuevoNodo ,mapa, obj.getMenu().getId());
						
					}else{
						
						if(mapa.get(obj.getMenu().getId()) != null && mapa.get(obj.getMenu().getId()).size() > 0)
						{//si es un Hijo, el cual tiene hijos => es Padre
							
							Node<PerfilMenu> objNuevoNodo = new Node<PerfilMenu>();
							
							ArrayList<PerfilMenu> lista = mapa.get(obj.getMenu().getId());
							
							boolean tieneHijoSeleccionado = false;
							
							if(lista != null)
							{
							
								for(PerfilMenu objPM : lista)
								{
									if(objPM.isSeleccionado())
									{
										tieneHijoSeleccionado = true;
										break;
									}
								}
							
							}
							
							obj.setSeleccionado(tieneHijoSeleccionado);
							
							objNuevoNodo.setData(obj);
							
							nodo.addChild(objNuevoNodo);
							
							arbolMenu(arbol,objNuevoNodo ,mapa, obj.getMenu().getId());
							
						}else{
							//No tiene hijos => No es padre.
							Node<PerfilMenu> objNuevoNodo = new Node<PerfilMenu>();
							
							objNuevoNodo.setData(obj);
							
							nodo.addChild(objNuevoNodo);

						}
						
					}
	
					
					
				}
				
				
			}
			
		}
		
	}

	/**
	 * Metodo que es invocado al dar clic en Guardar
	 * @return
	 */
	public String guardarPerfil(){
		
		String pagina = "";
		
		List<Node<PerfilMenu>> lista = arbol.toList();
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
		
		
		if(verificaSeleccionado(lista))
		{
			if(perfilBS.verificaDescripcionPerfil(objPerfil) == false)
			{

				lista.remove(0);//Se remueve la raiz (elemento que tiene objPerfilMenu.setId(-1))
				
				perfilBS.guardaPerfil(lista, getObjPerfil());
				
				
//				Flash flash = facesContext.getExternalContext().getFlash();
//				flash.setKeepMessages(true);
//				flash.setRedirect(true);
//
//				facesContext.addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_INFO, "", bundle.getString("altaPerfil.mensaje.exitoAgregar.parte1") + " " + getObjPerfil().getDescripcion()+ " " +bundle.getString("altaPerfil.mensaje.exitoAgregar.parte2")));
//				
				Flash flash = obtenerFlash();
				
				flash.setKeepMessages(true);
				flash.setRedirect(true);
				
				mensajes(resourceBundleEtiquetas("altaPerfil.mensaje.exitoAgregar.parte1")  + " " + getObjPerfil().getDescripcion()+ " " + resourceBundleEtiquetas("altaPerfil.mensaje.exitoAgregar.parte2"), FacesMessage.SEVERITY_INFO);
				
				pagina = "/adminPerfil/busquedaPerfil.xhtml?faces-redirect=true";
			
			}else{
				
				facesContext.addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", bundle.getString("altaPerfil.mensaje.descripcionExiste")));
				
				pagina = "";
			}
		}else{

			facesContext.addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", bundle.getString("altaPerfil.mensaje.seleccioneOpcion")));
			
			pagina = "";
		}
			
		
		
		return pagina;
	}
	
	/**
	 * Metodo para verificar que por lo menos haya un elemento seleccionado (checkbox)
	 * al momento de guardar o modificar.
	 * @param lista
	 * @return
	 */
	public boolean verificaSeleccionado(List<Node<PerfilMenu>> lista)
	{//Se verifica que haya por lo menos una opcion seleccionada
		
		boolean hayOpcionSeleccionada = false;
		
		for(Node<PerfilMenu>  objPerfilMenu : lista)
		{
			if(objPerfilMenu.getData().isSeleccionado() == true)
			{
				hayOpcionSeleccionada = true;
				break;
			}
		}
		
		return hayOpcionSeleccionada;
		
	}

	
	/**
	 * Metodo para inicializar los datos de alta.
	 */
	public void inicializarCamposAlta(){
		
		//Inicializa Perfil
		this.setObjPerfil(new Perfil());		
		this.getObjPerfil().setActivo(true);
		
		//Obtiene Padre e Hijos
		HashMap<Integer, ArrayList<PerfilMenu>> mapa = perfilBS.obtenerMenu();

		//Se crea objeto para colocarlo como raiz, pero no debe tener ningun uso.
		PerfilMenu objPerfilMenu = new PerfilMenu();
		
		objPerfilMenu.setId(-1);
		
		//Se crea nodo raiz
		nodoRaiz = new Node<PerfilMenu>();
		
		//Se inicializa nodo raiz
		nodoRaiz.setData(objPerfilMenu);
		
		//Se establece como root
		arbol.setRootElement(nodoRaiz);
		
		//Se crea el arbol para desplegarlo en altaPerfil.xhtml
		arbolMenu(arbol, nodoRaiz, mapa, 0);
		
		setEditable(false);
		
	}

	public void setObjPerfil(Perfil objPerfil) {
		this.objPerfil = objPerfil;
	}

	public Perfil getObjPerfil() {
		return objPerfil;
	}
	
	public Node<PerfilMenu> getNodoRaiz() {
		return nodoRaiz;
	}

	public void setNodoRaiz(Node<PerfilMenu> nodoRaiz) {
		this.nodoRaiz = nodoRaiz;
	}
	
	public String getUlOpcionOpen(){
		return "<ul style='display:inline-block;list-style-type: none;position:absolute;right:10%;'>";
	}
	
	public String getUlOpenTree(){
		return "<ul class=\'treeList\' style='list-style-type: none;margin-top:15px;'>";
	}

	public String getUlOpen(){
		return "<ul style='list-style-type: none;margin-top:15px;'>";
	}
	
	public String getUlClose(){
		return "</ul>";
	}
	
	public String getUlTituloOpen(){
		return "<ul style='list-style-type: none;background-color: white;padding-top: 5px;border-bottom: solid 1px gray;-webkit-border-bottom-right-radius: 5px;-webkit-border-bottom-left-radius: 5px;-moz-border-radius-bottomright: 5px;-moz-border-radius-bottomleft: 5px;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;'>";
	}
	
	public String getLiTituloOpen(){
		return "<li style='position:relative;border-radius: 5px;background-color: #c4c4c4;border-top: solid 1px gray;border-left: solid 1px gray;border-right: solid 1px gray;'>";
	}
	
	public String getLiSubTituloOpen(){
		return "<li style='position:relative;background-color: #c4c4c4;-webkit-border-bottom-right-radius: 5px;-webkit-border-bottom-left-radius: 5px;-moz-border-radius-bottomright: 5px;-moz-border-radius-bottomleft: 5px;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;'>";
	}
	
	public String getUlSubTituloOpen(){
		return "<ul style='list-style-type: none;background-color: white;-webkit-border-bottom-right-radius: 5px;-webkit-border-bottom-left-radius: 5px;-moz-border-radius-bottomright: 5px;-moz-border-radius-bottomleft: 5px;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;'>";
	}
	
	public String getLiOpen(){
		return "<li style='position:relative;padding-top: 5px;padding-bottom: 5px;'>";
	}
	
	public String getLiOpenOperations(){
		return "<li style='position:relative;'>";
	}
	
	public String getLiClose(){
		return "</li>";
	}

	public String getToolTipBusqueda(){
		return "<b>Especialista:</b> busca perfiles que tiene acceso a todos los convenios y plazas.<br/><b>No especialista:</b> busca perfiles que tienen personalizado sus convenios y plazas. <br/><b>Todos:</b> muestra todos los perfiles.";
	}


	public IPerfilBS getPerfilBS() {
		return perfilBS;
	}


	public void setPerfilBS(IPerfilBS perfilBS) {
		this.perfilBS = perfilBS;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isEditable() {
		return editable;
	}

}
