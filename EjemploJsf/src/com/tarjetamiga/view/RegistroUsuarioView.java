package com.tarjetamiga.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import com.tarjetamiga.bo.Usuario;

@ManagedBean
@ViewScoped
public class RegistroUsuarioView implements Serializable{
	

	private static final long serialVersionUID = -607949826904454598L;
	
	private Usuario objUsuario;
	private String password;
	private String confirmarPassword;
	private String valorPrueba;
	
	private String variableActionListener;
	private String mensajeAjax;
	
	@PostConstruct
	public void inicializa(){
		setObjUsuario(new Usuario());
		setPassword("");
		setConfirmarPassword("");
		System.out.println("Entro al postconstructor......");
	}
	
	public String registrar(){
		String pagina = "";
		
		System.out.println("Entro aqui metodo registrar al final: ");
		
		if(getPassword().equals(getConfirmarPassword()))
		{
			getObjUsuario().setPassword(getPassword());
			//pagina = "inicio?faces-redirect=true";
			pagina = "inicio";
		}else{
			FacesContext.getCurrentInstance().addMessage("form:mensajes", new FacesMessage("Confirmación de contraseña no coincide", ""));
		}
		
		return pagina;
	}
	
	
	
	
	

	public void obtenerValor(ActionEvent event){
		String idComponente = event.getComponent().getClientId();
		//System.out.println("1.- Entro aqui  "+valor);
		System.out.println("Entro al metodo ActionListener en el ManagedBean.");
		//setValorPrueba(valor);
	}
	
	
	
	
	
	
	
	public void manejadorEvento(AjaxBehaviorEvent event) {
		
		System.out.println("...Entro al evento AJAX...: ");
		
        setMensajeAjax("Esto fue una peticion Ajax");
    }
	
	
	
	
	
	
	public void ajaxEnInput(){
		setMensajeAjax("Entro al metodo ajax del input: "+getObjUsuario().getNombre()+" ... "+getObjUsuario().getApellidoPaterno());
	}
	
	
	
	
	
	
	
	
	
	
	

	public Usuario getObjUsuario() {
		return objUsuario;
	}

	public void setObjUsuario(Usuario objUsuario) {
		this.objUsuario = objUsuario;
	}

	public String getConfirmarPassword() {
		return confirmarPassword;
	}

	public void setConfirmarPassword(String confirmarPassword) {
		this.confirmarPassword = confirmarPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getValorPrueba() {
		return valorPrueba;
	}

	public void setValorPrueba(String valorPrueba) {
		this.valorPrueba = valorPrueba;
	}

	public String getMensajeAjax() {
		return mensajeAjax;
	}

	public void setMensajeAjax(String mensajeAjax) {
		this.mensajeAjax = mensajeAjax;
	}

	public String getVariableActionListener() {
		return variableActionListener;
	}

	public void setVariableActionListener(String variableActionListener) {
		System.out.println("Entro al setVariableActionListener del ManagedBean");
		this.variableActionListener = variableActionListener;
	}

	

}
