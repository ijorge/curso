package com.tarjetamiga.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.common.AbstractView;
import com.tarjetamiga.common.PermisoOpcionMenu;
import com.tarjetamiga.service.IPerfilBS;



@ManagedBean
@ViewScoped
public class PerfilBusquedaView extends AbstractView implements Serializable{
	
	static final Logger logger = LogManager.getLogger(PerfilBusquedaView.class.getName());
	
	private static final long serialVersionUID = -4545652798246132838L;
	private static final String PERFIL_SELECCIONADO = "perfilSeleccionado";
	private static final String EDITABLE = "editable";
	
	@ManagedProperty(value="#{perfilBS}")
	private IPerfilBS perfilBS;
	
	//Pantalla busqueda de perfil
	private ArrayList<Perfil> lstPerfil;
	private String descripcion;
	private int tipoPerfil;
	private Perfil perfilSeleccionado;
	private List<Perfil> perfilesSeleccionados;
	
	@PostConstruct
	public void init(){
		
		subir("temp", "cadena");
		logger.fatal("esto es un mensaje de tipo: fatal");
		logger.error("esto es un mensaje de tipo: error");
		logger.warn("esto es un mensaje de tipo: warn");
		logger.info("esto es un mensaje de tipo: info");
		logger.debug("esto es un mensaje de tipo: debug");
		inicializarCamposBusqueda();
		
	}

	public String altaPerfil() {
		
		return "/adminPerfil/altaPerfil.xhtml?faces-redirect=true";
		
	}

	public void eliminarPerfil() {
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
		
		if(getPerfilesSeleccionados() != null && getPerfilesSeleccionados().size() > 0)
		{

			StringBuffer sb = new StringBuffer();
			
			for(Perfil objPerfil : getPerfilesSeleccionados())
			{
				if(perfilBS.verificaPerfilEnUso(objPerfil) == false)
				{
					perfilBS.eliminarPerfil(objPerfil);
				}else{
					sb.append(objPerfil.getDescripcion()+"<br/>");
				}

			}
			

			if(sb.length() > 0)
			{
				facesContext.addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_WARN, "Precauci�n:", bundle.getString("busquedaPerfil.mensaje.precaucionEliminar") +sb.toString()));
			}
			
			
			this.lstPerfil = perfilBS.obtenerListaPerfil(getDescripcion());
			
		}else{
			//Mensaje
			facesContext.addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", bundle.getString("busquedaPerfil.mensaje.errorSeleccionar")));
		}
		

	}

	/**
	 * Inicializa los campos de la pantalla de busqueda.
	 * @param habilitaBotonAgregar
	 * @param habilitaBotonModificar
	 * @param habilitaBotonConsultar
	 * @param habilitaBotonEliminar
	 */
	public void inicializarCamposBusqueda(){
		this.descripcion = "";
		this.setTipoPerfil(0);
		this.setLstPerfil(new ArrayList<Perfil>());
		
		habilitaAcciones();
		

	    String from = obtenerFlash().get(FROM) != null ? (String)obtenerFlash().get(FROM) : null;
		
	    if(from != null)
	    {//Si viene de alta  o  modificar.
	    	buscarPerfil();
	    }
		
	}


	public void setLstPerfil(ArrayList<Perfil> lstPerfil) {
		this.lstPerfil = lstPerfil;
	}

	public ArrayList<Perfil> getLstPerfil() {
		return lstPerfil;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	
	public void buscarPerfil() {
		
		this.lstPerfil = perfilBS.obtenerListaPerfil(getDescripcion().toUpperCase());
		
	}
	
	public String consultarDetalle(){
		
		Flash flash = FacesContext.getCurrentInstance().                
        getExternalContext().getFlash();

		flash.put(PERFIL_SELECCIONADO, getPerfilSeleccionado());
		flash.put(EDITABLE, true);//Variable para habilitar checkbox (false.- permite editar, true.- no permite editar)
		
		
		return "/adminPerfil/consultaDetallePerfil.xhtml?faces-redirect=true";
	}
	
	public String editarDetalle(){
		
		Flash flash = FacesContext.getCurrentInstance().                
        getExternalContext().getFlash();

		flash.put(PERFIL_SELECCIONADO, getPerfilSeleccionado());
		flash.put(EDITABLE, false);//Variable para habilitar checkbox (false.- permite editar, true.- no permite editar)
		
		return "/adminPerfil/modificaDetallePerfil.xhtml?faces-redirect=true";
	}

	

	public void setPerfilSeleccionado(Perfil perfilSeleccionado) {
		this.perfilSeleccionado = perfilSeleccionado;
	}

	public Perfil getPerfilSeleccionado() {
		return perfilSeleccionado;
	}

	public void setPerfilesSeleccionados(List<Perfil> perfilesSeleccionados) {
		this.perfilesSeleccionados = perfilesSeleccionados;
	}

	public List<Perfil> getPerfilesSeleccionados() {
		return perfilesSeleccionados;
	}
	
	public String getUlOpcionOpen(){
		return "<ul style='display:inline-block;list-style-type: none;position:absolute;right:10%;'>";
	}
	
	public String getUlOpenTree(){
		return "<ul class=\'treeList\' style='list-style-type: none;margin-top:15px;'>";
	}

	public String getUlOpen(){
		return "<ul style='list-style-type: none;margin-top:15px;'>";
	}
	
	public String getUlClose(){
		return "</ul>";
	}
	
	public String getUlTituloOpen(){
		return "<ul style='list-style-type: none;background-color: white;border-bottom: solid 1px gray;-webkit-border-bottom-right-radius: 5px;-webkit-border-bottom-left-radius: 5px;-moz-border-radius-bottomright: 5px;-moz-border-radius-bottomleft: 5px;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;'>";
	}
	
	public String getLiTituloOpen(){
		return "<li style='position:relative;border-radius: 5px;background-color: #c4c4c4;border-top: solid 1px gray;border-left: solid 1px gray;border-right: solid 1px gray;'>";
	}
	
	public String getLiSubTituloOpen(){
		return "<li style='position:relative;background-color: #c4c4c4;'>";
	}
	
	public String getUlSubTituloOpen(){
		return "<ul style='list-style-type: none;background-color: white;-webkit-border-bottom-right-radius: 5px;-webkit-border-bottom-left-radius: 5px;-moz-border-radius-bottomright: 5px;-moz-border-radius-bottomleft: 5px;border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;'>";
	}
	
	public String getLiOpen(){
		return "<li style='position:relative;'>";
	}
	
	public String getLiClose(){
		return "</li>";
	}

	public void setTipoPerfil(int tipoPerfil) {
		this.tipoPerfil = tipoPerfil;
	}

	public int getTipoPerfil() {
		return tipoPerfil;
	}

	public String getToolTipBusqueda(){
		return "<b>Especialista:</b> busca perfiles que tiene acceso a todos los convenios y plazas.<br/><b>No especialista:</b> busca perfiles que tienen personalizado sus convenios y plazas. <br/><b>Todos:</b> muestra todos los perfiles.";
	}

	public IPerfilBS getPerfilBS() {
		return perfilBS;
	}

	public void setPerfilBS(IPerfilBS perfilBS) {
		this.perfilBS = perfilBS;
	}

}
