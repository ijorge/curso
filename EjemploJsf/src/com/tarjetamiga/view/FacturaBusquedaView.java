package com.tarjetamiga.view;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tarjetamiga.bo.Convenio;
import com.tarjetamiga.bo.Estatus;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.service.ISolicitudFacturaBS;

@ManagedBean
@ViewScoped
public class FacturaBusquedaView implements Serializable{
	
	static final Logger logger = LogManager.getLogger(FacturaBusquedaView.class.getName());
	
	private SolicitudFactura objSolicitudFactura;
	private List<SolicitudFactura> lstSolicitudFacturas;
	private List<Convenio> lstConvenio;
	private int idSolicitud;
	private List<Estatus> lstEstatus;
	private Date fechaCreacion;
	
	private List<SolicitudFactura> lstSolicitudFacturasSeleccionadas;
	
	@ManagedProperty(value="#{solicitudFacturaBS}")
	private ISolicitudFacturaBS objSolicitudFacturaBS;
	
	@PostConstruct
	public void inicializar(){
		setIdSolicitud(0);
		setFechaCreacion(new Date());
		setLstConvenio(new ArrayList<Convenio>());
		setObjSolicitudFactura(new SolicitudFactura());
		setLstSolicitudFacturas(new ArrayList<SolicitudFactura>());
		//Inicializa lista para facturas que se eliminaran
		setLstSolicitudFacturasSeleccionadas(new ArrayList<SolicitudFactura>());
		
		//Inicia llenado de lista de convenios.
		Convenio objConvenio = new Convenio();
		objConvenio.setId(1);
		objConvenio.setDescripcion("Seven Eleven");
		
		getLstConvenio().add(objConvenio);
		
		objConvenio = new Convenio();
		objConvenio.setId(2);
		objConvenio.setDescripcion("Oxxo");
		
		getLstConvenio().add(objConvenio);
		
		objConvenio = new Convenio();
		objConvenio.setId(3);
		objConvenio.setDescripcion("Soriana");
		
		getLstConvenio().add(objConvenio);
		//Termina llenado de lista de convenios.
		
		logger.info("entro info");
		logger.debug("entro debug");
		logger.warn("entro warn");
		
	}
	
	public void buscar(){
		
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String fechaString = formatter.format(getFechaCreacion());
		
		getObjSolicitudFactura().setFechaCreacion(fechaString);
		
		setLstSolicitudFacturas(getObjSolicitudFacturaBS().obtenerListaSolicitudFactura(getObjSolicitudFactura()));
		
		for(int i = 0 ; i < 200; i++)
		{
			logger.info("esto es un mensaje de tipo: info");
			logger.debug("esto es un mensaje de tipo: debug");
			logger.error("esto es un mensaje de tipo: error");
			logger.warn("esto es un mensaje de tipo: warn");
			logger.fatal("esto es un mensaje de tipo: fatal");
		}
		
	}
	
	public String editar(int idSolicitud){
		
		String pagina = "editarSolicitudFactura";
		
		SolicitudFactura objSolicitudFactura = null;
		
		for(int i=0 ; i < getLstSolicitudFacturas().size(); i++)
		{
			objSolicitudFactura = getLstSolicitudFacturas().get(i);
			
			if(objSolicitudFactura.getId() == idSolicitud)
			{
				break;
			}
			
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("objSolicitudFactura", objSolicitudFactura);
		
		return pagina;
	}
	
	
	public void eliminar(){
		
		if(getLstSolicitudFacturasSeleccionadas().size() != 0)
		{
			for(int i = 0 ; i < getLstSolicitudFacturasSeleccionadas().size();i++)
			{
				SolicitudFactura objSolicitudSeleccionada = getLstSolicitudFacturasSeleccionadas().get(i);
				
				for(int j = 0; j < getLstSolicitudFacturas().size(); j++)
				{
					if(objSolicitudSeleccionada.getId() == getLstSolicitudFacturas().get(j).getId())
					{
						getLstSolicitudFacturas().remove(j);
					}
				}
				
			}
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage("mensajeFor", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Para eliminar debe de seleccionar por lo menos una solicitud.", "") );
		}
		
		
		
	}

	public List<SolicitudFactura> getLstSolicitudFacturas() {
		return lstSolicitudFacturas;
	}

	public void setLstSolicitudFacturas(List<SolicitudFactura> lstSolicitudFacturas) {
		this.lstSolicitudFacturas = lstSolicitudFacturas;
	}

	public int getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(int idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public List<Estatus> getLstEstatus() {
		return lstEstatus;
	}

	public void setLstEstatus(List<Estatus> lstEstatus) {
		this.lstEstatus = lstEstatus;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public SolicitudFactura getObjSolicitudFactura() {
		return objSolicitudFactura;
	}

	public void setObjSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		this.objSolicitudFactura = objSolicitudFactura;
	}

	public List<Convenio> getLstConvenio() {
		return lstConvenio;
	}

	public void setLstConvenio(List<Convenio> lstConvenio) {
		this.lstConvenio = lstConvenio;
	}

	public List<SolicitudFactura> getLstSolicitudFacturasSeleccionadas() {
		return lstSolicitudFacturasSeleccionadas;
	}

	public void setLstSolicitudFacturasSeleccionadas(
			List<SolicitudFactura> lstSolicitudFacturasSeleccionadas) {
		this.lstSolicitudFacturasSeleccionadas = lstSolicitudFacturasSeleccionadas;
	}

	public ISolicitudFacturaBS getObjSolicitudFacturaBS() {
		return objSolicitudFacturaBS;
	}

	public void setObjSolicitudFacturaBS(ISolicitudFacturaBS objSolicitudFacturaBS) {
		this.objSolicitudFacturaBS = objSolicitudFacturaBS;
	}

}
