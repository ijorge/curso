package com.tarjetamiga.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.tarjetamiga.common.AbstractView;

@ManagedBean
@ViewScoped
public class GraficaVentaView extends AbstractView implements Serializable{

	private static final long serialVersionUID = 9022214127749997152L;
	
	private LineChartModel zoomModel;
//	private PieChartModel pieModel1;
	private BarChartModel barModel;
	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public HorizontalBarChartModel getHorizontalBarModel() {
		return horizontalBarModel;
	}

	public void setHorizontalBarModel(HorizontalBarChartModel horizontalBarModel) {
		this.horizontalBarModel = horizontalBarModel;
	}

	private HorizontalBarChartModel horizontalBarModel;
	 
    @PostConstruct
    public void init() {
        createZoomModel();
        createBarModels();
    }
 
    public LineChartModel getZoomModel() {
        return zoomModel;
    }
     
    private void createZoomModel() {
    	
        zoomModel = initLinearModel();
        zoomModel.setTitle("Ventas anuales (Zoom)");
        zoomModel.setZoom(true);
        zoomModel.setLegendPosition("e");  // n s e w
        
        DateAxis axis = new DateAxis("Meses");
        axis.setTickAngle(-50);
        axis.setMax("2014-07-01");
        axis.setTickFormat("%b %#d, %y");
        //axis.setTickFormat("%b");// Solo mes
        zoomModel.getAxes().put(AxisType.X, axis);
        
        Axis yAxis = zoomModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(12);
        
    }
     
    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();
 
        
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Wii");
 
        series1.set("2014-01-01", 2);
        series1.set("2014-02-01", 1);
        series1.set("2014-03-01", 3);
        series1.set("2014-04-01", 6);
        series1.set("2014-05-01", 8);
 
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("PS 3");
 
        series2.set("2014-01-01", 6);
        series2.set("2014-02-01", 3);
        series2.set("2014-03-01", 2);
        series2.set("2014-04-01", 7);
        series2.set("2014-05-01", 9);
        
        LineChartSeries series3 = new LineChartSeries();
        series3.setLabel("Xbox 360");
 
        series3.set("2014-01-01", 4);
        series3.set("2014-02-01", 1);
        series3.set("2014-03-01", 1);
        series3.set("2014-04-01", 6);
        series3.set("2014-05-01", 7);
 
        model.addSeries(series1);
        model.addSeries(series2);
        model.addSeries(series3);
         
        return model;
    }
    
    
    
    ///////////////////Barras
    
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 120);
        boys.set("2005", 100);
        boys.set("2006", 44);
        boys.set("2007", 150);
        boys.set("2008", 25);
 
        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 110);
        girls.set("2007", 135);
        girls.set("2008", 120);
 
        model.addSeries(boys);
        model.addSeries(girls);
         
        return model;
    }
     
    private void createBarModels() {
        createBarModel();
        createHorizontalBarModel();
    }
     
    private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Grafica de barra");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Genero");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Nacimientos");
        yAxis.setMin(0);
        yAxis.setMax(200);
    }
     
    private void createHorizontalBarModel() {
        horizontalBarModel = new HorizontalBarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Hombres");
        boys.set("2004", 50);
        boys.set("2005", 96);
        boys.set("2006", 44);
        boys.set("2007", 55);
        boys.set("2008", 25);
 
        ChartSeries girls = new ChartSeries();
        girls.setLabel("Mujeres");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 82);
        girls.set("2007", 35);
        girls.set("2008", 120);
 
        horizontalBarModel.addSeries(boys);
        horizontalBarModel.addSeries(girls);
         
        horizontalBarModel.setTitle("Titulo de la grafica horizontal");
        horizontalBarModel.setLegendPosition("e");
        horizontalBarModel.setStacked(true);
         
        Axis xAxis = horizontalBarModel.getAxis(AxisType.X);
        xAxis.setLabel("Nacimientos");
        xAxis.setMin(0);
        xAxis.setMax(200);
         
        Axis yAxis = horizontalBarModel.getAxis(AxisType.Y);
        yAxis.setLabel("Genero");        
    }
    
    
    
    
    
 
	
}
