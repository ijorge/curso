package com.tarjetamiga.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.Convenio;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.service.ISolicitudFacturaBS;

@ManagedBean
@ViewScoped
public class FacturaEditarView {
	
	private SolicitudFactura objSolicitudFactura;
	private Collection<Convenio> lstConvenio;
	
	private Concepto objConcepto;
	private Collection<Concepto> lstConcepto;
	
	@ManagedProperty(value="#{solicitudFacturaBS}")
	private ISolicitudFacturaBS objSolicitudFacturaBS;

	@PostConstruct
	public void inicializar(){
		
		setObjConcepto(new Concepto());
		
		setLstConvenio(new ArrayList<Convenio>());
		
		setLstConcepto(new ArrayList<Concepto>());
		
		setObjSolicitudFactura(new SolicitudFactura());
		
		
		SolicitudFactura objSolicitudFacturaLocal = null;
		
		objSolicitudFacturaLocal = (SolicitudFactura) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("objSolicitudFactura");
		
		
		SolicitudFactura objSolicitudFacturaTemporal = getObjSolicitudFacturaBS().obtenerDetalleSolicitudFactura(objSolicitudFacturaLocal.getId());
		
		setObjSolicitudFactura(objSolicitudFacturaTemporal);
		
		
		List<Concepto> lstConcepto = getObjSolicitudFacturaBS().obtenerDetalleConcepto(objSolicitudFacturaLocal.getId());
		
		setLstConcepto(lstConcepto);
		
		
		//Inicia llenado de lista de convenios.
			Convenio objConvenio = new Convenio();
			objConvenio.setId(1);
			objConvenio.setDescripcion("Seven Eleven");
			
			getLstConvenio().add(objConvenio);
			
			objConvenio = new Convenio();
			objConvenio.setId(2);
			objConvenio.setDescripcion("Oxxo");
			
			getLstConvenio().add(objConvenio);
			
			objConvenio = new Convenio();
			objConvenio.setId(3);
			objConvenio.setDescripcion("Soriana");
			
			getLstConvenio().add(objConvenio);
		//Termina llenado de lista de convenios.
		
	}
	
	public String modificar(){
		String pagina = "busquedaSolicitudFactura.xhtml?faces-redirect=true";
		
		 FacesContext context = FacesContext.getCurrentInstance();
		 
		 getObjSolicitudFacturaBS().modificarSolicitudFactura(getObjSolicitudFactura(), getLstConcepto());
         
	     context.addMessage("mensajeGrowl", new FacesMessage(FacesMessage.SEVERITY_INFO, "Exito: Se modifico de forma correcta el registro: "+getObjSolicitudFactura().getDescripcion(),  "") );
		
		return pagina;
	}
	
	public void agregar(){
		getLstConcepto().add(getObjConcepto());
		setObjConcepto(new Concepto());
	}
	
	public void eliminar(Concepto objConcepto){
		getLstConcepto().remove(objConcepto);
	}

	public SolicitudFactura getObjSolicitudFactura() {
		return objSolicitudFactura;
	}

	public void setObjSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		this.objSolicitudFactura = objSolicitudFactura;
	}

	public Collection<Convenio> getLstConvenio() {
		return lstConvenio;
	}

	public void setLstConvenio(Collection<Convenio> lstConvenio) {
		this.lstConvenio = lstConvenio;
	}

	public Concepto getObjConcepto() {
		return objConcepto;
	}

	public void setObjConcepto(Concepto objConcepto) {
		this.objConcepto = objConcepto;
	}

	public Collection<Concepto> getLstConcepto() {
		return lstConcepto;
	}

	public void setLstConcepto(Collection<Concepto> lstConcepto) {
		this.lstConcepto = lstConcepto;
	}

	public ISolicitudFacturaBS getObjSolicitudFacturaBS() {
		return objSolicitudFacturaBS;
	}

	public void setObjSolicitudFacturaBS(ISolicitudFacturaBS objSolicitudFacturaBS) {
		this.objSolicitudFacturaBS = objSolicitudFacturaBS;
	}
	
}
