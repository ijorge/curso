package com.tarjetamiga.view;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import com.tarjetamiga.bo.Usuario;

@ManagedBean
@ViewScoped
public class CopyOfRegistroUsuarioView implements Serializable{
	

	private static final long serialVersionUID = -607949826904454598L;
	
	private Usuario objUsuario;
	private String password;
	private String confirmarPassword;
	private String mensaje;
	private Date fecha;
	private String valorPrueba;
	
	private String variableActionListener;
	private String mensajeAjax;
	
	@PostConstruct
	public void inicializa(){
		setObjUsuario(new Usuario());
		setFecha(new Date());
	}
	
	public String registrar(){
		String pagina = "";
		
		System.out.println("Entro aqui al final: "+getValorPrueba());
		
		if(getPassword().equals(getConfirmarPassword()))
		{
			pagina = "exito";
		}else{
			FacesContext.getCurrentInstance().addMessage("form:mensajes", new FacesMessage("Confirmación de contraseña no coincide", ""));
		}
		
		return pagina;
	}
	
	public void obtenerValor(String valor){
		System.out.println("Entro aqui 1: "+valor);
		setValorPrueba(valor);
	}
	
	public void manejadorEvento(AjaxBehaviorEvent event) {
		
		System.out.println("...Entro al evento AJAX...: ");
		
        setMensajeAjax("Esto fue una peticion Ajax");
    }

	public Usuario getObjUsuario() {
		return objUsuario;
	}

	public void setObjUsuario(Usuario objUsuario) {
		this.objUsuario = objUsuario;
	}

	public String getConfirmarPassword() {
		return confirmarPassword;
	}

	public void setConfirmarPassword(String confirmarPassword) {
		this.confirmarPassword = confirmarPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getValorPrueba() {
		return valorPrueba;
	}

	public void setValorPrueba(String valorPrueba) {
		this.valorPrueba = valorPrueba;
	}

	public String getVariableActionListener() {
		return variableActionListener;
	}

	public void setVariableActionListener(String variableActionListener) {
		System.out.println("...Variable Action Listener");
		this.variableActionListener = variableActionListener;
	}

	public String getMensajeAjax() {
		return mensajeAjax;
	}

	public void setMensajeAjax(String mensajeAjax) {
		this.mensajeAjax = mensajeAjax;
	}

}
