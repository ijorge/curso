package com.tarjetamiga.view;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.primefaces.event.MenuActionEvent;
import org.primefaces.model.menu.DefaultMenuItem;

import com.tarjetamiga.common.PermisoOpcionMenu;

@ManagedBean
@ViewScoped
public class MenuView {

	private static final String BOTONES_SESSION = "BOTONES";
	
	public String clicOpcionMenu(ActionEvent ev) throws IOException {
		
		MenuActionEvent j = (MenuActionEvent) ev; //cast para obtener las propiedades 
        DefaultMenuItem itm = (DefaultMenuItem) j.getMenuItem(); // el menu item al que se dio clic        
        String url = itm.getParams().get("url").get(0); // parametros del item
        
        boolean habilitaBotonAgregar = new Boolean (itm.getParams().get("agregar").get(0)).booleanValue();
		boolean habilitaBotonModificar = new Boolean (itm.getParams().get("modificar").get(0)).booleanValue();
		boolean habilitaBotonConsultar = new Boolean (itm.getParams().get("consultar").get(0)).booleanValue();
		boolean habilitaBotonEliminar = new Boolean (itm.getParams().get("eliminar").get(0)).booleanValue();
        
		PermisoOpcionMenu objControl = new PermisoOpcionMenu();
		objControl.setHabilitaBotonAgregar(habilitaBotonAgregar);
		objControl.setHabilitaBotonConsultar(habilitaBotonConsultar);
		objControl.setHabilitaBotonModificar(habilitaBotonModificar);
		objControl.setHabilitaBotonEliminar(habilitaBotonEliminar);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		session.setAttribute(BOTONES_SESSION, objControl);
		
		
		return url;
	}
	

}
