package com.tarjetamiga.listener.debug;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;


public class CiclioVidaListener implements PhaseListener{

	@Override
	public void afterPhase(PhaseEvent event) {
		System.out.println("TERMINA FASE = " + event.getPhaseId());
		
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		System.out.println("EMPIEZA FASE = " + event.getPhaseId());
		
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}
