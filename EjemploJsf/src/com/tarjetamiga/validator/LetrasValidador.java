package com.tarjetamiga.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "ValidaLetras")
public class LetrasValidador implements Validator{

	private static final String SOLO_LETRAS = "^[A-Za-z ]*$";
 
	private Pattern pattern;
	private Matcher matcher;
 
	public LetrasValidador(){
		  pattern = Pattern.compile(SOLO_LETRAS);
	}
 
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
 
		System.out.println("Entro a validar letras y espacios.....");
		
		matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
 
			String mensaje = obtenerEtiqueta(component) +" " + "Solo se permiten letra y espacios.";
			
			FacesMessage msg = 	new FacesMessage(mensaje, mensaje);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
 
		}
 
	}
	
	protected String obtenerEtiqueta(UIComponent uiComponent) {
        String campo = (String) uiComponent.getAttributes().get("etiqueta");

        if(campo == null) {
            campo = "" ;
        }

        return campo;
    }

}
