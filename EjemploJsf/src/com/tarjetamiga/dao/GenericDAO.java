package com.tarjetamiga.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, Id extends Serializable> {

	T buscarPorId(Id id);
	List<T> buscarTodos();
	void guardar(T objeto);
	void borrar(T objeto);
	
}
