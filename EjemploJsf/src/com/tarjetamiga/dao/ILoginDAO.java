package com.tarjetamiga.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;


public interface ILoginDAO extends GenericDAO<Usuario,String>{

	public boolean validaUsuario(Usuario objUsuario);
	
	public boolean validaUsuarioSP(Usuario objUsuario);
	
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu(Usuario objUsuario);
	
}
