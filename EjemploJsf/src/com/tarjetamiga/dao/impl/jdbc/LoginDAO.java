package com.tarjetamiga.dao.impl.jdbc;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.dao.ILoginDAO;
import com.tarjetamiga.dao.mapper.PerfilMenuResultSetExtractor;
import com.tarjetamiga.dao.mapper.UsuarioRowMapper;


@Repository
public class LoginDAO implements ILoginDAO{
	
	@Autowired
	private DataSource dataSource;

	@Override
	public boolean validaUsuario(Usuario objUsuario) {
		
		boolean booExiste = false;
		
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT id, nombre, apellido_paterno, apellido_materno, nombre_usuario, password FROM usuario ");
		sql.append("WHERE nombre_usuario = ? and password = md5(?)");
		
		Usuario objUsuarioLocal = (Usuario) new JdbcTemplate(dataSource).queryForObject(sql.toString(), 
				new Object[] { objUsuario.getNombreUsuario(), objUsuario.getPassword() }, 
				new UsuarioRowMapper());
		
		if(objUsuarioLocal.getId() != 0)
		{
			booExiste = true;
		}
		
		return booExiste;
	}
	
	@Override
	public boolean validaUsuarioSP(Usuario objUsuario){
		
		boolean booExiste = false;
		
		SpConsultarUsuario objSpConsultarUsuario = new  SpConsultarUsuario(dataSource);
		
		Collection<Usuario> lstUsuarioLocal = objSpConsultarUsuario.execute(objUsuario);
				
		if(lstUsuarioLocal != null && lstUsuarioLocal.size() != 0)
		{
			Usuario objUsuarioLocal = (Usuario) lstUsuarioLocal.toArray()[0];
			if(objUsuarioLocal.getId() != 0)
			{
				booExiste = true;
			}
			
		}
		
		return booExiste;
	}
	
	private final class SpConsultarUsuario extends StoredProcedure {
        private SpConsultarUsuario(DataSource dataSource) {
            super(dataSource, "curso.obtener_usuario");
            
            declareParameter(new SqlReturnResultSet("RESULT_LIST", new UsuarioRowMapper()));
            declareParameter(new SqlParameter("p_nombre_usuario", Types.VARCHAR));
            declareParameter(new SqlParameter("p_password", Types.VARCHAR));

            
            compile();
        }   
        
		@SuppressWarnings("unchecked")
		public Collection<Usuario> execute(Usuario objUsuario) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("p_nombre_usuario", objUsuario.getNombreUsuario());
            parameters.put("p_password", objUsuario.getPassword());
            Map<?,?> resultado = execute(parameters);
            
            return resultado.get("RESULT_LIST") != null ? (Collection<Usuario>)resultado.get("RESULT_LIST") : null;
            
        }
    }
	
	
	
	/////Obtener menu dinamico
	
	
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu(Usuario objUsuario) {

		SpConsultarMenuDinamico objSpConsultarMenuDinamico = new SpConsultarMenuDinamico(dataSource);
		
		return objSpConsultarMenuDinamico.execute(objUsuario);
		
	}
	
	
	private final class SpConsultarMenuDinamico extends StoredProcedure {
        private SpConsultarMenuDinamico(DataSource dataSource) {
            super(dataSource, "curso.sppro_obtenerMenu");
            
            PerfilMenuResultSetExtractor objDirectorioResultSetExtractor = new PerfilMenuResultSetExtractor();
                        
            declareParameter(new SqlParameter("p_usuario", Types.VARCHAR));
            
            //Se creo un ResultSetExtractor.
            declareParameter(new SqlReturnResultSet("RESULT_LIST", objDirectorioResultSetExtractor));
            
            compile();
        }   
        
		@SuppressWarnings("unchecked")
		public HashMap<Integer, ArrayList<PerfilMenu>> execute(Usuario objUsuario) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("p_usuario", objUsuario.getNombreUsuario());
            parameters.put("p_id_aplicacion", 1);
            
            Map<?,?> resultado = execute(parameters);
            
            return resultado.containsKey("RESULT_LIST") ? (HashMap<Integer, ArrayList<PerfilMenu>>)resultado.get("RESULT_LIST") : null;
            
        }
    }
	
	
	//////////Termina obtener menu dinamico
	
	
	

	@Override
	public Usuario buscarPorId(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> buscarTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardar(Usuario objeto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borrar(Usuario objeto) {
		// TODO Auto-generated method stub
		
	}

}
