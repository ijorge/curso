package com.tarjetamiga.dao.impl.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.stereotype.Repository;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.dao.ISolicitudFacturaDAO;
import com.tarjetamiga.dao.mapper.SolicitudFacturaRowMapper;

@Repository
public class SolicitudFacturaCallableDAO implements ISolicitudFacturaDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudFactura> obtenerListaSolicitudFactura(
			final SolicitudFactura objSolicitudFactura) {
		
		final String procedureCall = "{call curso.sppro_consultasolicitudfactura(?, ?, ?, ?)}";
		
		SqlReturnResultSet cursor = new SqlReturnResultSet("CURSOR", new SolicitudFacturaRowMapper());
		SqlParameter facturaId = new SqlParameter(Types.INTEGER);
		SqlParameter convenioId = new SqlParameter(Types.INTEGER);
		SqlParameter importeTotal = new SqlParameter(Types.VARCHAR);
		SqlParameter fechaCreacion = new SqlParameter(Types.DATE);


		List<SqlParameter> paramList = new ArrayList<SqlParameter>();
		
		paramList.add(facturaId );
		paramList.add(convenioId );
		paramList.add(importeTotal);
		paramList.add(fechaCreacion);
		paramList.add(cursor);
		
		Map<String, Object> resultMap = jdbcTemplate.call(new CallableStatementCreator() 
		{

			@Override
			public CallableStatement createCallableStatement(Connection connection)
			throws SQLException {

							CallableStatement callableStatement = connection.prepareCall(procedureCall);
							callableStatement.setInt(1, objSolicitudFactura.getObjConvenio().getId());
							callableStatement.setInt(2, objSolicitudFactura.getObjConvenio().getId());
							callableStatement.setString(3, objSolicitudFactura.getImporteTotal());
							callableStatement.setString(4, objSolicitudFactura.getFechaCreacion());
							
							return callableStatement;
				
							}
			
			}, paramList);
		
		List<SolicitudFactura> lstSolicitudFactura = null;
		
		if(resultMap.get("CURSOR") != null)
        {
        	lstSolicitudFactura = (List<SolicitudFactura>) resultMap.get("CURSOR");
        }
		
		return lstSolicitudFactura;
	}

	@Override
	public int insertarSolicitud(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertarConcepto(SolicitudFactura objSolicitudFactura,
			Concepto objConcepto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarConceptos(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertarArchivo(Archivo objArchivo,
			SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}


}
