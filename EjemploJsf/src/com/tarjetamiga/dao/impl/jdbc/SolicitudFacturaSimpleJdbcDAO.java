package com.tarjetamiga.dao.impl.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.dao.ISolicitudFacturaDAO;
import com.tarjetamiga.dao.mapper.SolicitudFacturaRowMapper;

@Repository
public class SolicitudFacturaSimpleJdbcDAO implements ISolicitudFacturaDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<SolicitudFactura> obtenerListaSolicitudFactura(
			SolicitudFactura objSolicitudFactura) {
		
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
		.withProcedureName("sppro_consultasolicitudfactura")
		.withSchemaName("curso")
		.declareParameters(new SqlReturnResultSet("CURSOR", new SolicitudFacturaRowMapper() ) );

		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("prsolicitud_factura_id", objSolicitudFactura.getId());
		inParamMap.put("prid_convenio", objSolicitudFactura.getObjConvenio().getId());
		inParamMap.put("primporte_total", objSolicitudFactura.getImporteTotal());
		inParamMap.put("prfecha_creacion", objSolicitudFactura.getFechaCreacion());
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);


		Map<String, Object> resultMap = simpleJdbcCall.execute(in);
		
		List<SolicitudFactura> lstSolicitudFactura = null;
		
		if(resultMap.get("CURSOR") != null)
        {
        	lstSolicitudFactura = (List<SolicitudFactura>) resultMap.get("CURSOR");
        }
		
		return lstSolicitudFactura;
	}

	@Override
	public int insertarSolicitud(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertarConcepto(SolicitudFactura objSolicitudFactura,
			Concepto objConcepto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminarConceptos(SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertarArchivo(Archivo objArchivo,
			SolicitudFactura objSolicitudFactura) {
		// TODO Auto-generated method stub
		
	}

}
