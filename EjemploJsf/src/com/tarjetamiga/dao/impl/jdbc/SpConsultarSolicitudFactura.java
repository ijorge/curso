package com.tarjetamiga.dao.impl.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.dao.mapper.SolicitudFacturaRowMapper;

public class SpConsultarSolicitudFactura extends StoredProcedure {
	
    private SpConsultarSolicitudFactura(DataSource dataSource) {
    	
        super(dataSource, "curso.sppro_consultasolicitudfactura");
                    
        declareParameter(new SqlParameter("prsolicitud_factura_id", Types.VARCHAR));
        declareParameter(new SqlParameter("prid_convenio", Types.INTEGER));
        declareParameter(new SqlParameter("primporte_total", Types.VARCHAR));
        declareParameter(new SqlParameter("prfecha_creacion", Types.DATE));
        declareParameter(new SqlReturnResultSet("RESULT_LIST", new SolicitudFacturaRowMapper()));
        
        compile();
    }   
    
	
	@SuppressWarnings("unchecked")
	public List<SolicitudFactura> execute(SolicitudFactura objSolicitudFactura) {
		
        Map<String, Object> parameters = new HashMap<String, Object>();  
        
        parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
        parameters.put("prid_convenio", objSolicitudFactura.getObjConvenio().getId());
        parameters.put("primporte_total", objSolicitudFactura.getImporteTotal());
        parameters.put("prfecha_creacion", objSolicitudFactura.getFechaCreacion());
        
        Map<?,?> resultado = execute(parameters);
        
        List<SolicitudFactura> lstSolicitudFactura = null;
        
        if(resultado.get("RESULT_LIST") != null)
        {
        	lstSolicitudFactura = (List<SolicitudFactura>) resultado.get("RESULT_LIST");
        }
        
        return lstSolicitudFactura;
        
    }
}
