package com.tarjetamiga.dao.impl.jdbc;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.common.nodo.Node;
import com.tarjetamiga.dao.IPerfilDAO;
import com.tarjetamiga.dao.mapper.MenuRowMapper;
import com.tarjetamiga.dao.mapper.PerfilMenuRowMapper;
import com.tarjetamiga.dao.mapper.PerfilRowMapper;

@Repository
public class PerfilDAO implements IPerfilDAO{
	
	@Autowired
	private DataSource dataSource;
	
	
	@SuppressWarnings("unchecked")
	@Override	
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu() {
		
	//Mapa que contiene como Key el elemento padre y como Object una lista de elementos hijos.
	LinkedHashMap<Integer, ArrayList<PerfilMenu>> mapa = new LinkedHashMap<Integer, ArrayList<PerfilMenu>>();
	
	//Se inicializa la lista de hijos.
	ArrayList<PerfilMenu> lista = new ArrayList<PerfilMenu>();

	SpObtenerMenu spObtenerMenu = new SpObtenerMenu(getDataSource());
	
	Map<String, Object> map =  spObtenerMenu.execute();
	
	Collection<Menu> collectionPerfilMenu =  map.containsKey("p_out_cur_resultado") ? (Collection<Menu>)map.get("p_out_cur_resultado") : null;
	
	
	Perfil objPerfil = new Perfil();
	
	for(Menu objMenu : collectionPerfilMenu)
	{
		PerfilMenu objPerfilMenu = new PerfilMenu();
		objPerfilMenu.setPerfil(objPerfil);
		objPerfilMenu.setMenu(objMenu);
		
		objPerfilMenu.setConsulta(false);
		objPerfilMenu.setAgregar(false);
		objPerfilMenu.setEditar(false);
		objPerfilMenu.setEliminar(false);
		
		if(mapa.isEmpty()){
			//Entra solo la primera vez, se agrega a la lista para guardar el hijo
			lista.add(objPerfilMenu);
			
			//Se agrega el idPadre.
			mapa.put(objPerfilMenu.getMenu().getPadre(), lista);
		}else{
			
			if( mapa.containsKey(objPerfilMenu.getMenu().getPadre())){
				//Si el mapa contiene el padre, se agrega a la lista el nuevo hijo
				
				//Se obtiene la lista de hijos del padre.
				ArrayList<PerfilMenu> listaDelMap = mapa.get(objPerfilMenu.getMenu().getPadre());
				
				//Se agrega el hijo a la lista.
				listaDelMap.add(objPerfilMenu);
			}else{
				//Si en el mapa no esta el padre, se crea nueva lista donde guardar sus hijos
				 lista = new ArrayList<PerfilMenu>();
				 
				 lista.add(objPerfilMenu);
				 
				 mapa.put(objPerfilMenu.getMenu().getPadre(), lista);
			}
				
		}
	}		
	 
		return mapa;
	}
	
	@Override	
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenuModificar(int idPerfil) {

	HashMap<Integer, ArrayList<PerfilMenu>> mapa = new HashMap<Integer, ArrayList<PerfilMenu>>();
	ArrayList<PerfilMenu> lista = new ArrayList<PerfilMenu>();
	
	SpObtenerDatosMenuPerfil spObtenerMenu = new SpObtenerDatosMenuPerfil(getDataSource());
	Collection<PerfilMenu> collectionPerfilMenu = spObtenerMenu.execute(idPerfil);

	
	for(PerfilMenu objPerfilMenu : collectionPerfilMenu)
	{
		
		boolean tieneHijoSeleccionado = false;
		
		if(objPerfilMenu.isAgregar() || objPerfilMenu.isConsulta() || objPerfilMenu.isEditar() || objPerfilMenu.isEliminar())
		{
			tieneHijoSeleccionado = true;
		}
		
		objPerfilMenu.setSeleccionado(tieneHijoSeleccionado);
	
	if(mapa.isEmpty()){
		//Si empieza, se agrega a la lista para guardar el hijo
		lista.add(objPerfilMenu);
		mapa.put(objPerfilMenu.getMenu().getPadre(), lista);
		
	}else{
		if( mapa.containsKey(objPerfilMenu.getMenu().getPadre())){
			//Si el mapa contiene el padre, se agrega a la lista el nuevo hijo
			ArrayList<PerfilMenu> listaDelMap = mapa.get(objPerfilMenu.getMenu().getPadre());
			listaDelMap.add(objPerfilMenu);
		}else{
			//Si en el mapa no esta el padre, se crea nueva lista donde guardar sus hijos
			 lista = new ArrayList<PerfilMenu>();
			 lista.add(objPerfilMenu);
			 mapa.put(objPerfilMenu.getMenu().getPadre(), lista);
		}
			
	}
	
	
	
		
	}

	 
		return mapa;
	}
	
	@Override
	public Perfil guardaPerfil(final Perfil objPerfil){
    	
    	SpCrearPerfil spCrearPerfil = new SpCrearPerfil(getDataSource());
    	
    	//int id = spCrearPerfil.execute(objPerfil.getDescripcion(), objPerfil.getEspecialista());
    	
    	int id = spCrearPerfil.execute(objPerfil.getDescripcion());
		
    	objPerfil.setId(id);
    	
    	return objPerfil;
	
	}
	
	@Override
	public int guardaPerfilMenu(List<Node<PerfilMenu>>  lstPerfilMenu, Perfil objPerfil){
		
		SpCrearPerfilMenu spCrearPerfilMenu = new SpCrearPerfilMenu(getDataSource());
		
		for(Node<PerfilMenu> objPerfilMenu : lstPerfilMenu)
		{
			if(objPerfilMenu.getData().isSeleccionado() == true)//Si por lo menos una opcion del menu puede por lo menos consultar, agregar, modificar ó eliminar.
			{
				spCrearPerfilMenu.execute(objPerfilMenu.getData(), objPerfil);
			}
		}
		
		return 0;
	}
	
	@Override
	public ArrayList<Perfil> obtenerListaPerfil(String descripcion) {
		
		SpObtenerDatosPerfil spObtenerDatosPerfil = new SpObtenerDatosPerfil(getDataSource());
		
		Collection<Perfil> collectionPerfilMenu = spObtenerDatosPerfil.execute(descripcion);
		
		return (ArrayList<Perfil>) collectionPerfilMenu;
	}
		
	@Override
	public Perfil actualizaPerfil(final Perfil objPerfil){

		SpEditarPerfil spEditarPerfil = new SpEditarPerfil(getDataSource());
		
		spEditarPerfil.execute(objPerfil);
    	
    	return objPerfil;
	
	}
	
	@Override
	public int eliminarDetallePerfilMenu(Perfil objPerfil) {
		
		SpEliminarPerfilDetalle spEliminarPerfilDetalle = new SpEliminarPerfilDetalle(getDataSource());
		
		spEliminarPerfilDetalle.execute(objPerfil);
		
		return 0;
	}
	
	@Override
	public int eliminarPerfil(Perfil objPerfil) {
		
		SpEliminarPerfil spEliminarPerfil = new SpEliminarPerfil(getDataSource());
		
		spEliminarPerfil.execute(objPerfil);
		
		return 0;
	}
	
	/**
	 * Metodo para validar si exite algun usuario relacionado al perfil.
	 */
	@Override
	public boolean verificaPerfilEnUso(Perfil objPerfil) {
		
		boolean existePerfilAsignado = false;
		
		SpValidaExisteUsuarioPerfil spValidaExisteUsuarioPerfil = new SpValidaExisteUsuarioPerfil(getDataSource());
		
		int cantidad = spValidaExisteUsuarioPerfil.execute(objPerfil.getId());
		
		if (cantidad > 0) {
		
			existePerfilAsignado = true;

		}
		
		return existePerfilAsignado;
	}
	
	/**
	 * Metodo para validar si un usuario ya tiene un perfil asignado.
	 */
	@Override
	public boolean verificaUsuarioConPerfil(Usuario objUsuario) {
		
		boolean usuarioYaExiste = false;
		
		SpValidaUsuarioPerfilAlta spValidaUsuarioPerfilAlta = new SpValidaUsuarioPerfilAlta(getDataSource());
		
		int cantidad = spValidaUsuarioPerfilAlta.execute(objUsuario.getId());
		
		if (cantidad > 0) {
		
			usuarioYaExiste = true;

		}
		
		return usuarioYaExiste;
	}
	
	
	
	
	@Override
	public boolean verificaDescripcionPerfil(Perfil objPerfil) {
		
		SpValidaExisteDescripcionPerfil spValidaDescripcionPerfil = new SpValidaExisteDescripcionPerfil(getDataSource());
		
		int cantidad = spValidaDescripcionPerfil.execute(objPerfil.getDescripcion(), objPerfil.getId());
	
		boolean existePerfil = false;
		
		if (cantidad > 0) {
		
			existePerfil = true;

		}
		
		return existePerfil;
	}
	
	@Override
	public Perfil obtenerPerfil(Usuario objUsuario){
		
		SpObtenerPerfil spObtenerPerfil = new SpObtenerPerfil(getDataSource());
		ArrayList<Perfil> lstPerfil = (ArrayList<Perfil>) spObtenerPerfil.execute(objUsuario);
		Perfil objPerfil = new Perfil();
		
		if (lstPerfil != null && lstPerfil.size() > 0) {
			objPerfil = lstPerfil.get(0);
		}
		
		return objPerfil;
		
	}
	
	public PerfilMenu obtenerPrivilegiosPorMenu(Perfil objPerfil, Menu objMenu){
		
		SpObtenerPrivilegiosPorMenu objSpObtenerPrivilegiosPorMenu = new SpObtenerPrivilegiosPorMenu(dataSource);
		Collection<PerfilMenu> lstCollection = objSpObtenerPrivilegiosPorMenu.execute(objPerfil.getId(), objMenu.getId());
		PerfilMenu objPerfilMenu;
		
		Iterator<PerfilMenu> iteLstPerfilMenu = lstCollection.iterator();
		
		if(lstCollection != null && iteLstPerfilMenu.hasNext())
		{
			objPerfilMenu = lstCollection.iterator().next();
		}else{
			objPerfilMenu = new PerfilMenu();
		}
		
		return objPerfilMenu;
	}
	

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Stores Procedures
	 * @author jorge.arrambide
	 *
	 */
	
	private final class SpObtenerMenu extends StoredProcedure {
        private SpObtenerMenu(DataSource dataSource) {
            super(dataSource, "curso.sppro_obtener_menu_creacion");

            MenuRowMapper rowMapper = new MenuRowMapper();
            declareParameter(new SqlReturnResultSet("p_out_cur_resultado", rowMapper));
            
            compile();
        }   
        
    }
	
	
	private final class SpObtenerDatosMenuPerfil extends StoredProcedure {
        private SpObtenerDatosMenuPerfil(DataSource dataSource) {
            super(dataSource, "curso.sppro_obtenerPerfilEditar");

            PerfilMenuRowMapper rowMapper = new PerfilMenuRowMapper();
            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
            declareParameter(new SqlReturnResultSet("p_out_cur_resultado", rowMapper));
            
            compile();
        }   
        
        @SuppressWarnings("unchecked")
		public Collection<PerfilMenu> execute(int idPerfil) {
            Map<String, Integer> parameters = new HashMap<String, Integer>();    
            parameters.put("p_id_perfil", idPerfil);
            
            Map<?,?> resultado = execute(parameters);
            return resultado.containsKey("p_out_cur_resultado") ? (Collection<PerfilMenu>)resultado.get("p_out_cur_resultado") : null;
        }
    }
	
	private final class SpCrearPerfil extends StoredProcedure {
        private SpCrearPerfil(DataSource dataSource) {
            super(dataSource, "curso.sppro_crear_perfil");
            declareParameter(new SqlParameter("descripcion", Types.VARCHAR));
            declareParameter(new SqlOutParameter("id_perfil", Types.INTEGER));
            
            compile();
        }   
        
        public int execute(String descripcion) {
            Map<String, Object> parameters = new HashMap<String, Object>();  
            parameters.put("descripcion", descripcion);
            
            Map<?,?> resultado = execute(parameters);
            return resultado.containsKey("id_perfil") ? (Integer)resultado.get("id_perfil") : 0;
        }
        
    }

	private final class SpCrearPerfilMenu extends StoredProcedure {
        private SpCrearPerfilMenu(DataSource dataSource) {
            super(dataSource, "curso.sppro_crear_perfil_menu");
            declareParameter(new SqlParameter("p_perfil_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_menu_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_consultar", Types.INTEGER));
            declareParameter(new SqlParameter("p_agregar", Types.INTEGER));
            declareParameter(new SqlParameter("p_modificar", Types.INTEGER));
            declareParameter(new SqlParameter("p_eliminar", Types.INTEGER));
            compile();
        }   
        
		public void execute(PerfilMenu objPerfilMenu, Perfil objPerfil) {
			
            Map<String, Integer> parameters = new HashMap<String, Integer>();  
            parameters.put("p_perfil_id", objPerfil.getId());
            parameters.put("p_menu_id", objPerfilMenu.getMenu().getId());
            
            parameters.put("p_consultar", (objPerfilMenu.isConsulta() == true ? 1 : 0));
            parameters.put("p_agregar", (objPerfilMenu.isAgregar() == true ? 1 : 0));
            parameters.put("p_modificar", (objPerfilMenu.isEditar() == true ? 1 : 0));
            parameters.put("p_eliminar", (objPerfilMenu.isEliminar() == true ? 1 : 0));
            
            execute(parameters);
            
        }
    }
	
	
	private final class SpObtenerDatosPerfil extends StoredProcedure {
        private SpObtenerDatosPerfil(DataSource dataSource) {
            super(dataSource, "curso.sppro_obtenerDatosPerfil");

            PerfilRowMapper rowMapper = new PerfilRowMapper();
            
            //Nota: Importa el orden de declaracion de parametros
            declareParameter(new SqlReturnResultSet("p_out_cur_resultado", rowMapper));
            declareParameter(new SqlParameter("p_descripcion", Types.VARCHAR));
            
            compile();
        }   
        
        @SuppressWarnings("unchecked")
		public Collection<Perfil> execute(String descripcion) {
            Map<String, Object> parameters = new HashMap<String, Object>();    
            parameters.put("p_descripcion", descripcion);
            
            Map<?,?> resultado = execute(parameters);
            return resultado.containsKey("p_out_cur_resultado") ? (Collection<Perfil>)resultado.get("p_out_cur_resultado") : null;
        }
    }
	
	private final class SpObtenerPerfil extends StoredProcedure {
        private SpObtenerPerfil(DataSource dataSource) {
            super(dataSource, "curso.sppro_obtenerPerfil");

            PerfilRowMapper rowMapper = new PerfilRowMapper();
            
            //Importa orden de declaracion de parametros
            declareParameter(new SqlReturnResultSet("p_out_cur_resultado", rowMapper));
            declareParameter(new SqlParameter("p_id_usuario", Types.INTEGER));
            declareParameter(new SqlParameter("p_id_aplicacion", Types.INTEGER));
            
            compile();
        }   
        
        @SuppressWarnings("unchecked")
		public Collection<Perfil> execute(Usuario objUsuario) {
            Map<String, Integer> parameters = new HashMap<String, Integer>();    
            parameters.put("p_id_usuario", objUsuario.getId());
            parameters.put("p_id_aplicacion", 1);
            
            Map<?,?> resultado = execute(parameters);
            return resultado.containsKey("p_out_cur_resultado") ? (Collection<Perfil>)resultado.get("p_out_cur_resultado") : null;
        }
    }
	
	
	 private final class SpEditarPerfil extends StoredProcedure {
	        private SpEditarPerfil(DataSource dataSource) {
	            super(dataSource, "curso.sppro_modificarPerfil");
	            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
	            declareParameter(new SqlParameter("p_descripcion", Types.VARCHAR)); 
	            compile();
	        }   
	        
	        public void execute(Perfil objPerfil) {
	            Map<String, Object> parameters = new HashMap<String, Object>();
	            parameters.put("p_id_perfil", objPerfil.getId());
	            parameters.put("p_descripcion", objPerfil.getDescripcion());
	            execute(parameters);
	        }
	    }
	 
	 
	 private final class SpEliminarPerfilDetalle extends StoredProcedure {
	        private SpEliminarPerfilDetalle(DataSource dataSource) {
	            super(dataSource, "curso.sppro_eliminarDetallePerfil");
	            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
	            compile();
	        }   
	        
	        public void execute(Perfil objPerfil) {
	            Map<String, Integer> parameters = new HashMap<String, Integer>();
	            parameters.put("p_id_perfil", objPerfil.getId());
	            execute(parameters);
	        }
	    }
	 
	 
	 private final class SpEliminarPerfil extends StoredProcedure {
	        private SpEliminarPerfil(DataSource dataSource) {
	            super(dataSource, "curso.sppro_eliminarPerfil");
	            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
	            compile();
	        }   
	        
	        public void execute(Perfil objPerfil) {
	            Map<String, Integer> parameters = new HashMap<String, Integer>();
	            parameters.put("p_id_perfil", objPerfil.getId());
	            execute(parameters);
	        }
	    }
	 
	 
	 private final class SpValidaExisteDescripcionPerfil extends StoredProcedure {
	        private SpValidaExisteDescripcionPerfil(DataSource dataSource) {
	            super(dataSource, "curso.sppro_valida_descripcion_perfil");
	            
	            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
	            declareParameter(new SqlParameter("p_descripcion", Types.VARCHAR));
	            declareParameter(new SqlOutParameter("p_out_resultado", Types.INTEGER));
	            
	            compile();
	        }   
	        
	        public int execute(String descripcion,int idPerfil) {
	            Map<String, Object> parameters = new HashMap<String, Object>();  
	            parameters.put("p_id_perfil", idPerfil);
	            parameters.put("p_descripcion", descripcion);
	            
	            Map<?, ?> resultado = execute(parameters);
	            return resultado.containsKey("p_out_resultado") ? (Integer)resultado.get("p_out_resultado") : null;
	        }
	    }
	 
	 private final class SpValidaExisteUsuarioPerfil extends StoredProcedure {
	        private SpValidaExisteUsuarioPerfil(DataSource dataSource) {
	            super(dataSource, "curso.sppro_validaExisteUsuarioPerfil");
	            
	            declareParameter(new SqlParameter("p_id_perfil", Types.VARCHAR));
	            declareParameter(new SqlOutParameter("p_out_resultado", Types.INTEGER));
	            declareParameter(new SqlParameter("p_id_aplicacion", Types.INTEGER));
	            
	            compile();
	        }   
	        
	        public int execute(int idPerfil) {
	            Map<String, Integer> parameters = new HashMap<String, Integer>();  
	            parameters.put("p_id_perfil", idPerfil);
	            parameters.put("p_id_aplicacion", 1);
	            
	            Map<?, ?> resultado = execute(parameters);
	            return resultado.containsKey("p_out_resultado") ? (Integer)resultado.get("p_out_resultado") : 0;
	        }
	    }
	 
	 
	 private final class SpValidaUsuarioPerfilAlta extends StoredProcedure {
	        private SpValidaUsuarioPerfilAlta(DataSource dataSource) {
	            super(dataSource, "curso.sppro_validaUsuarioPerfilAlta");
	            
	            declareParameter(new SqlParameter("p_id_usuario", Types.INTEGER));
	            declareParameter(new SqlOutParameter("p_out_resultado", Types.INTEGER));
	            declareParameter(new SqlParameter("p_id_aplicacion", Types.INTEGER));
	            
	            compile();
	        }   
	        
	        public int execute(int idUsuario) {
	            Map<String, Integer> parameters = new HashMap<String, Integer>();  
	            parameters.put("p_id_usuario", idUsuario);
	            parameters.put("p_id_aplicacion", 1);
	            
	            Map<?, ?> resultado = execute(parameters);
	            return resultado.containsKey("p_out_resultado") ? (Integer)resultado.get("p_out_resultado") : 0;
	        }
	    }
	 
	 
	 private final class SpObtenerPrivilegiosPorMenu extends StoredProcedure {
	        private SpObtenerPrivilegiosPorMenu(DataSource dataSource) {
	            super(dataSource, "alpha.PCK_PERFIL.Obtener_Privilegios_Por_Menu");

	            PerfilMenuRowMapper rowMapper = new PerfilMenuRowMapper();
	            declareParameter(new SqlParameter("p_id_perfil", Types.INTEGER));
	            declareParameter(new SqlParameter("p_id_menu", Types.INTEGER));
	            declareParameter(new SqlParameter("p_id_aplicacion", Types.INTEGER));
	            declareParameter(new SqlReturnResultSet("p_out_cur_resultado", rowMapper));
	            
	            compile();
	        }   
	        
	        @SuppressWarnings("unchecked")
			public Collection<PerfilMenu> execute(int idPerfil, int idMenu) {
	            Map<String, Integer> parameters = new HashMap<String, Integer>();    
	            parameters.put("p_id_perfil", idPerfil);
	            parameters.put("p_id_menu", idMenu);
	            parameters.put("p_id_aplicacion", 1);
	            
	            Map<?,?> resultado = execute(parameters);
	            return resultado.containsKey("p_out_cur_resultado") ? (Collection<PerfilMenu>)resultado.get("p_out_cur_resultado") : null;
	        }
	    }
	
	
	

}
