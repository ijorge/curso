package com.tarjetamiga.dao.impl.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.dao.ISolicitudFacturaDAO;
import com.tarjetamiga.dao.mapper.ConceptoRowMapper;
import com.tarjetamiga.dao.mapper.SolicitudFacturaRowMapper;

@Repository
public class SolicitudFacturaDAO implements ISolicitudFacturaDAO{
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public List<SolicitudFactura> obtenerListaSolicitudFactura(SolicitudFactura objSolicitudFactura){
		
		SpConsultarSolicitudFactura objSpConsultarSolicitudFactura = new SpConsultarSolicitudFactura(dataSource);
		
		List<SolicitudFactura> lstSolicitud = objSpConsultarSolicitudFactura.execute(objSolicitudFactura);
		
		return lstSolicitud;
		
	}
	
	public int insertarSolicitud(SolicitudFactura objSolicitudFactura)
	{
		
		SpInsertarSolicitudFactura objSpInsertarSolicitudFactura = new SpInsertarSolicitudFactura(dataSource);
		
		List<SolicitudFactura> lstSolicitud = objSpInsertarSolicitudFactura.execute(objSolicitudFactura);
		
		if(lstSolicitud!= null && lstSolicitud.size() > 0)
		{
			objSolicitudFactura.setId(lstSolicitud.get(0).getId());
		}
		
		return 0;
		
	}
	
	public int insertarConcepto(SolicitudFactura objSolicitudFactura, Concepto objConcepto)
	{
		
		SpInsertarConcepto objSpInsertarConcepto = new SpInsertarConcepto(dataSource);
		objSpInsertarConcepto.execute(objConcepto, objSolicitudFactura);
				
		return 0;
		
	}
	
	@Override
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura) {
		
		SpConsultarDetalleSolicitudFactura objSpConsultarDetalleSolicitudFactura = new SpConsultarDetalleSolicitudFactura(dataSource);
		List<SolicitudFactura> lstSolicitudFactura = objSpConsultarDetalleSolicitudFactura.execute(idSolcitudFactura);
		
		SolicitudFactura obj = new SolicitudFactura();
		
		if(lstSolicitudFactura != null && lstSolicitudFactura.size() > 0)
		{
			obj = lstSolicitudFactura.get(0);
		}
		
		return obj;
	}
	

	
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura) {
		SpConsultarDetalleConcepto objSpConsultarDetalleConcepto = new SpConsultarDetalleConcepto(dataSource);
		
		return objSpConsultarDetalleConcepto.execute(idSolcitudFactura);
	}
	
	
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura){
		SpModificarSolicitudFactura objSpModificarSolicitudFactura = new SpModificarSolicitudFactura(dataSource);
		objSpModificarSolicitudFactura.execute(objSolicitudFactura);
		
	}
	
	public void eliminarConceptos(SolicitudFactura objSolicitudFactura){
		SpEliminarConceptos objSpEliminarConceptos = new SpEliminarConceptos(dataSource);
		objSpEliminarConceptos.execute(objSolicitudFactura);
	}
	
	public void insertarArchivo(Archivo objArchivo, SolicitudFactura objSolicitudFactura){
		
		SpInsertarArchivo objSpInsergarArchivo = new SpInsertarArchivo(dataSource);
		objSpInsergarArchivo.execute(objArchivo, objSolicitudFactura);
		
	}
	
	
	
	private final class SpConsultarSolicitudFactura extends StoredProcedure {
		
        private SpConsultarSolicitudFactura(DataSource dataSource) {
        	
            super(dataSource, "curso.sppro_consultasolicitudfactura1");
                        
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.VARCHAR));
            declareParameter(new SqlParameter("prid_convenio", Types.INTEGER));
            declareParameter(new SqlParameter("primporte_total", Types.VARCHAR));
            declareParameter(new SqlParameter("prfecha_creacion", Types.DATE));
            declareParameter(new SqlReturnResultSet("RESULT_LIST", new SolicitudFacturaRowMapper()));
            
            compile();
        }   
        
		
		@SuppressWarnings("unchecked")
		public List<SolicitudFactura> execute(SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
            parameters.put("prid_convenio", objSolicitudFactura.getObjConvenio().getId());
            parameters.put("primporte_total", objSolicitudFactura.getImporteTotal());
            parameters.put("prfecha_creacion", objSolicitudFactura.getFechaCreacion());
            
            Map<?,?> resultado = execute(parameters);
            
            List<SolicitudFactura> lstSolicitudFactura = null;
            
            if(resultado.get("RESULT_LIST") != null)
            {
            	lstSolicitudFactura = (List<SolicitudFactura>) resultado.get("RESULT_LIST");
            }
            
            return lstSolicitudFactura;
            
        }
    }
	
	
	private final class SpInsertarSolicitudFactura extends StoredProcedure {
		
        private SpInsertarSolicitudFactura(DataSource dataSource) {
            super(dataSource, "curso.sppro_insertasolicitudfactura");
            
            declareParameter(new SqlReturnResultSet("RESULT_LIST", new SolicitudFacturaRowMapper()));
            
            declareParameter(new SqlParameter("prdescripcion", Types.VARCHAR));
            declareParameter(new SqlParameter("primporte_total", Types.VARCHAR));
            declareParameter(new SqlParameter("prid_estatus", Types.INTEGER));
            declareParameter(new SqlParameter("prfecha_creacion", Types.DATE));
            declareParameter(new SqlParameter("prid_convenio", Types.INTEGER));
            
            compile();
        }   
        
		
		
		@SuppressWarnings("unchecked")
		public List<SolicitudFactura> execute(SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prdescripcion", objSolicitudFactura.getDescripcion());
            parameters.put("primporte_total", objSolicitudFactura.getImporteTotal());
            parameters.put("prid_estatus", objSolicitudFactura.getObjEstatus().getId());
            parameters.put("prfecha_creacion", objSolicitudFactura.getFechaCreacion());
            parameters.put("prid_convenio", objSolicitudFactura.getObjConvenio().getId());
            
            Map<?,?> resultado = execute(parameters);
            
            return resultado.get("RESULT_LIST") != null ? (List<SolicitudFactura>)resultado.get("RESULT_LIST") : null;
            
        }
    }
	
	
	private final class SpInsertarConcepto extends StoredProcedure {
		
        private SpInsertarConcepto(DataSource dataSource) {
            super(dataSource, "curso.sppro_insertaconcepto");
                        
            declareParameter(new SqlParameter("prdescripcion", Types.VARCHAR));
            declareParameter(new SqlParameter("primporte", Types.VARCHAR));
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.INTEGER));
            
            compile();
        }   
        
		
		
		public void execute(Concepto objConcepto, SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prdescripcion", objConcepto.getDescripcion());
            parameters.put("primporte", objConcepto.getImporte());
            parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
            
            execute(parameters);
            
            
        }
    }


	
	
	private final class SpConsultarDetalleSolicitudFactura extends StoredProcedure {
		
        private SpConsultarDetalleSolicitudFactura(DataSource dataSource) {
        	
            super(dataSource, "curso.spcon_detalle_factura");
                        
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.INTEGER));
            declareParameter(new SqlReturnResultSet("RESULT_LIST", new SolicitudFacturaRowMapper()));
            
            compile();
        }   
        
		
		@SuppressWarnings("unchecked")
		public List<SolicitudFactura> execute(int idSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prsolicitud_factura_id", idSolicitudFactura);
            
            Map<?,?> resultado = execute(parameters);
            
            List<SolicitudFactura> lstSolicitudFactura = null;
            
            if(resultado.get("RESULT_LIST") != null)
            {
            	lstSolicitudFactura = (List<SolicitudFactura>) resultado.get("RESULT_LIST");
            }
            
            return lstSolicitudFactura;
            
        }
    }
	
	
	private final class SpConsultarDetalleConcepto extends StoredProcedure {
		
        private SpConsultarDetalleConcepto(DataSource dataSource) {
        	
            super(dataSource, "curso.sppro_consultaconcepto");
                        
            declareParameter(new SqlParameter("prid", Types.INTEGER));
            declareParameter(new SqlReturnResultSet("RESULT_LIST", new ConceptoRowMapper()));
            
            compile();
        }   
        
		
		@SuppressWarnings("unchecked")
		public List<Concepto> execute(int idSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prid", idSolicitudFactura);
            
            Map<?,?> resultado = execute(parameters);
            
            List<Concepto> lstConcepto = null;
            
            if(resultado.get("RESULT_LIST") != null)
            {
            	lstConcepto = (List<Concepto>) resultado.get("RESULT_LIST");
            }
            
            return lstConcepto;
            
        }
    }
	
	
	
	
	
	private final class SpModificarSolicitudFactura extends StoredProcedure {
		
        private SpModificarSolicitudFactura(DataSource dataSource) {
        	
            super(dataSource, "curso.sppro_modificasolicitudfactura");
                        
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.INTEGER));
            declareParameter(new SqlParameter("prid_convenio", Types.INTEGER));
            declareParameter(new SqlParameter("prdescripcion", Types.VARCHAR));
            declareParameter(new SqlParameter("primporte_total", Types.VARCHAR));
            declareParameter(new SqlParameter("prestatus", Types.INTEGER));
            
            compile();
            
        }   
        
				
		public void execute(SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
            parameters.put("prid_convenio", objSolicitudFactura.getObjConvenio().getId());
            parameters.put("prdescripcion", objSolicitudFactura.getDescripcion());
            parameters.put("primporte_total", objSolicitudFactura.getImporteTotal());
            parameters.put("prestatus", objSolicitudFactura.getObjEstatus().getId());
            
            execute(parameters);
                      
            
        }
    }
	
	
	
	
	private final class SpEliminarConceptos extends StoredProcedure {
		
        private SpEliminarConceptos(DataSource dataSource) {
        	
            super(dataSource, "curso.sppro_eliminaconcepto");
                        
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.INTEGER));
            
            compile();
            
        }   
        
				
		public void execute(SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
            
            execute(parameters);
                      
            
        }
    }


	
	private final class SpInsertarArchivo extends StoredProcedure {
		
        private SpInsertarArchivo(DataSource dataSource) {
        	
            super(dataSource, "curso.sppro_insertaarchivo");
                        
            declareParameter(new SqlParameter("prnombre_archivo", Types.VARCHAR));
            declareParameter(new SqlParameter("prarchivo", Types.BLOB));
            declareParameter(new SqlParameter("prsolicitud_factura_id", Types.INTEGER));
            
            compile();
            
        }   
        
				
		public void execute(Archivo objArchivo, SolicitudFactura objSolicitudFactura) {
			
            Map<String, Object> parameters = new HashMap<String, Object>();  
            
            parameters.put("prnombre_archivo", objArchivo.getNombreArchivo());
            parameters.put("prarchivo", objArchivo.getArrBytesArchivo());
            parameters.put("prsolicitud_factura_id", objSolicitudFactura.getId());
            
            execute(parameters);
                      
            
        }
    }
	
	
}
