package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.Usuario;


public class UsuarioRowMapper implements RowMapper<Usuario>{

	@Override
	public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = rs.getMetaData();
        
        int numberOfColumns = rsMetaData.getColumnCount();
        
        String[] columnasResultSet = new String[numberOfColumns];
        // get the column names; column indexes start from 1
        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
		
		Usuario objUsuario = new Usuario();
		
		if(listaColumnasResultSet.contains("id")){
			objUsuario.setId(rs.getInt("id"));
		}
		
		if(listaColumnasResultSet.contains("nombre")){
			objUsuario.setNombre(rs.getString("nombre"));
		}
		
		if(listaColumnasResultSet.contains("apellido_paterno")){
			objUsuario.setApellidoPaterno(rs.getString("apellido_paterno"));
		}
		
		if(listaColumnasResultSet.contains("apellido_materno")){
			objUsuario.setApellidoMaterno(rs.getString("apellido_materno"));
		}
		
		if(listaColumnasResultSet.contains("nombre_usuario")){
			objUsuario.setNombreUsuario(rs.getString("nombre_usuario"));
		}
		
		return objUsuario;
	}

}

