package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.Concepto;

public class ConceptoRowMapper implements RowMapper<Concepto>{

	@Override
	public Concepto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = rs.getMetaData();
        
        int numberOfColumns = rsMetaData.getColumnCount();
        
        String[] columnasResultSet = new String[numberOfColumns];

        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
        
        Concepto objConcepto = new Concepto();
        
        if(listaColumnasResultSet.contains("id")){
        	objConcepto.setId(rs.getInt("id"));
        }
        
        if(listaColumnasResultSet.contains("descripcion")){
        	objConcepto.setDescripcion(rs.getString("descripcion"));
        }
        
        if(listaColumnasResultSet.contains("importe")){
        	objConcepto.setImporte(rs.getString("importe"));
        }
        
        if(listaColumnasResultSet.contains("solicitud_factura_id")){
        	objConcepto.getObjSolicitudFactura().setId(rs.getInt("solicitud_factura_id"));
        }
		
		return objConcepto;
	}

}
