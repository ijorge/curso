package  com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;

public class PerfilMenuRowMapper implements RowMapper<PerfilMenu>{

	@Override
	public PerfilMenu mapRow(ResultSet resultSet, int line) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = resultSet.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        String[] columnasResultSet = new String[numberOfColumns];
        // get the column names; column indexes start from 1
        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
        
        PerfilMenu objPerfilMenu = new PerfilMenu();
        Menu objMenu = new Menu();
        Perfil objPerfil = new Perfil();

        //Menu
        
        if(listaColumnasResultSet.contains("m_id")){
        	objMenu.setId(resultSet.getInt("m_id"));
        }  
        
        if(listaColumnasResultSet.contains("m_descripcion")){
        	objMenu.setDescripcion(resultSet.getString("m_descripcion"));
        }
        
        if(listaColumnasResultSet.contains("m_url")){
        	objMenu.setUrl(resultSet.getString("m_url"));
        }
        
        if(listaColumnasResultSet.contains("m_metodo")){
        	objMenu.setMetodo(resultSet.getString("m_metodo"));
        }
        
        if(listaColumnasResultSet.contains("m_activo")){
        	objMenu.setActivo(resultSet.getBoolean("m_activo"));
        }
        
        if(listaColumnasResultSet.contains("m_padre")){
        	objMenu.setPadre(resultSet.getInt("m_padre"));
        }
        
        //Perfil
        
        if(listaColumnasResultSet.contains("p_id")){
        	objPerfil.setId(resultSet.getInt("p_id"));
        } 
        
        if(listaColumnasResultSet.contains("p_descripcion")){
        	objPerfil.setDescripcion(resultSet.getString("p_descripcion"));
        } 
        
        if(listaColumnasResultSet.contains("p_activo")){
        	objPerfil.setActivo(resultSet.getBoolean("p_activo"));
        }
        
        
        //PerfilMenu
        
        if(listaColumnasResultSet.contains("pm_id")){
        	objPerfilMenu.setId(resultSet.getInt("pm_id"));
        }  
        
        if(listaColumnasResultSet.contains("pm_agregar")){
        	objPerfilMenu.setAgregar(resultSet.getBoolean("pm_agregar"));
        }
        
        if(listaColumnasResultSet.contains("pm_consultar")){
        	objPerfilMenu.setConsulta(resultSet.getBoolean("pm_consultar"));
        }
        
        if(listaColumnasResultSet.contains("pm_modificar")){
        	objPerfilMenu.setEditar(resultSet.getBoolean("pm_modificar"));
        }
        
        if(listaColumnasResultSet.contains("pm_eliminar")){
        	objPerfilMenu.setEliminar(resultSet.getBoolean("pm_eliminar"));
        }
           
        objPerfilMenu.setMenu(objMenu);
        objPerfilMenu.setPerfil(objPerfil);

	  
		return objPerfilMenu;
	}

}
