package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.SolicitudFactura;

public class SolicitudFacturaRowMapper implements RowMapper<SolicitudFactura>{

	@Override
	public SolicitudFactura mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = rs.getMetaData();
        
        int numberOfColumns = rsMetaData.getColumnCount();
        
        String[] columnasResultSet = new String[numberOfColumns];

        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
        
        SolicitudFactura objSolicitudFactura = new SolicitudFactura();
        
        if(listaColumnasResultSet.contains("id")){
        	objSolicitudFactura.setId(rs.getInt("id"));
        }
        
        if(listaColumnasResultSet.contains("descripcion")){
        	objSolicitudFactura.setDescripcion(rs.getString("descripcion"));
        }
        
        if(listaColumnasResultSet.contains("importe_total")){
        	objSolicitudFactura.setImporteTotal(rs.getString("importe_total"));
        }
        
        if(listaColumnasResultSet.contains("id_estatus")){
        	objSolicitudFactura.getObjEstatus().setId(rs.getInt("id_estatus"));
        }
        
        if(listaColumnasResultSet.contains("fecha_creacion")){
        	objSolicitudFactura.setFechaCreacion(rs.getString("fecha_creacion"));
        }
        
        if(listaColumnasResultSet.contains("id_convenio")){
        	objSolicitudFactura.getObjConvenio().setId(rs.getInt("id_convenio"));
        }
        
		
		return objSolicitudFactura;
	}

}
