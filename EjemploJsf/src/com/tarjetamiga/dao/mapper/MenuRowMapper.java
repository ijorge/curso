package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.Menu;


public class MenuRowMapper implements RowMapper<Menu>{

	@Override
	public Menu mapRow(ResultSet resultSet, int line) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = resultSet.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        String[] columnasResultSet = new String[numberOfColumns];
        // get the column names; column indexes start from 1
        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
        
        Menu objMenu = new Menu();
        
        if(listaColumnasResultSet.contains("id")){
        	objMenu.setId(resultSet.getInt("id"));
        }  
        
        if(listaColumnasResultSet.contains("descripcion")){
        	objMenu.setDescripcion(resultSet.getString("descripcion"));
        }
        
        if(listaColumnasResultSet.contains("url")){
        	objMenu.setUrl(resultSet.getString("url"));
        }
        
        if(listaColumnasResultSet.contains("activo")){
        	objMenu.setActivo(resultSet.getBoolean("activo"));
        }
        
        if(listaColumnasResultSet.contains("padre")){
        	objMenu.setPadre(resultSet.getInt("padre"));
        }
        
		return objMenu;
	}

}
