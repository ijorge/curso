package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;

public class PerfilMenuResultSetExtractor implements ResultSetExtractor<Map<Integer, ArrayList<PerfilMenu>>>{

	@Override
	public Map<Integer, ArrayList<PerfilMenu>> extractData(ResultSet rs)
			throws SQLException, DataAccessException {
		
		HashMap<Integer, ArrayList<PerfilMenu>> mapaMenuHijos = new HashMap<Integer, ArrayList<PerfilMenu>>();
		
		ArrayList<PerfilMenu> listaMenuHijos = new ArrayList<PerfilMenu>();
		
		while (rs.next()) {
			
			PerfilMenu objPerfilMenu = new PerfilMenu();
			
			Perfil objPerfil = new Perfil();
			objPerfil.setId((rs.getInt("p_id")));
			objPerfil.setDescripcion(rs.getString("p_descripcion"));
		
			Menu objMenu = new Menu();
			objMenu.setId((rs.getInt("m_id")));
			objMenu.setDescripcion(rs.getString("m_descripcion"));
			objMenu.setMetodo(rs.getString("m_metodo"));
			objMenu.setPadre((rs.getInt("m_padre")));
			objMenu.setUrl(rs.getString("m_url"));		
			
			objPerfilMenu.setMenu(objMenu);
			objPerfilMenu.setPerfil(objPerfil);
			
			objPerfilMenu.setAgregar((rs.getInt("pm_agregar")) != 0);
			objPerfilMenu.setEditar((rs.getInt("pm_modificar")) != 0);
			objPerfilMenu.setConsulta((rs.getInt("pm_consultar")) != 0);
			objPerfilMenu.setEliminar((rs.getInt("pm_eliminar")) != 0);

				if(mapaMenuHijos.isEmpty()){
					//Si empieza, se agrega a la lista para guardar el hijo
					listaMenuHijos.add(objPerfilMenu);
					mapaMenuHijos.put((rs.getInt("m_padre")), listaMenuHijos);
				}else{
					if( mapaMenuHijos.containsKey((rs.getInt("m_padre")))){
						//Si el mapa contiene el padre, se agrega a la lista el nuevo hijo
						ArrayList<PerfilMenu> listaDelMap = mapaMenuHijos.get((rs.getInt("m_padre")));
						listaDelMap.add(objPerfilMenu);
					}else{
						//Si en el mapa no esta el padre, se crea nueva lista donde guardar sus hijos
						 listaMenuHijos = new ArrayList<PerfilMenu>();
						 listaMenuHijos.add(objPerfilMenu);
						 mapaMenuHijos.put((rs.getInt("m_padre")), listaMenuHijos);
					}
						
				}

			
		}
		
		return mapaMenuHijos;
	}

}
