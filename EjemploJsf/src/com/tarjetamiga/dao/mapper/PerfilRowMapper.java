package com.tarjetamiga.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.tarjetamiga.bo.Perfil;


public class PerfilRowMapper implements RowMapper<Perfil>{

	@Override
	public Perfil mapRow(ResultSet resultSet, int line) throws SQLException {
		
		//Obtener los nombres de las columnas que contiene el resultSet
        ResultSetMetaData rsMetaData = resultSet.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        String[] columnasResultSet = new String[numberOfColumns];
        // get the column names; column indexes start from 1
        for (int j = 1; j < numberOfColumns + 1; j++) {
            String columnName = rsMetaData.getColumnName(j);
            columnasResultSet[j-1] = columnName; 
        }
        
        List<String> listaColumnasResultSet = Arrays.asList(columnasResultSet);
        
        Perfil objPerfil = new Perfil();
        
        if(listaColumnasResultSet.contains("id")){
        	objPerfil.setId(resultSet.getInt("id"));
        } 
        
        if(listaColumnasResultSet.contains("descripcion")){
        	objPerfil.setDescripcion(resultSet.getString("descripcion"));
        } 
        
		
		return objPerfil;
	}

}
