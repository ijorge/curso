package com.tarjetamiga.dao;

import java.util.List;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;

public interface ISolicitudFacturaDAO {
	
	public List<SolicitudFactura> obtenerListaSolicitudFactura(SolicitudFactura objSolicitudFactura);
	
	public int insertarSolicitud(SolicitudFactura objSolicitudFactura);
	
	public int insertarConcepto(SolicitudFactura objSolicitudFactura, Concepto objConcepto);
	
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura);
	
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura);
	
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura);
	
	public void eliminarConceptos(SolicitudFactura objSolicitudFactura);
	
	public void insertarArchivo(Archivo objArchivo, SolicitudFactura objSolicitudFactura);


}
