package com.tarjetamiga.bo;


public class Archivo {

	private int id;
	private String nombreArchivo;
	private byte [] arrBytesArchivo;
	private SolicitudFactura objSolicitudFactura;
	
	public Archivo(){
		this.id = 0;
		this.nombreArchivo = "";
		this.arrBytesArchivo = null;
		this.objSolicitudFactura = new SolicitudFactura();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


	public SolicitudFactura getObjSolicitudFactura() {
		return objSolicitudFactura;
	}

	public void setObjSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		this.objSolicitudFactura = objSolicitudFactura;
	}

	public byte [] getArrBytesArchivo() {
		return arrBytesArchivo;
	}

	public void setArrBytesArchivo(byte [] arrBytesArchivo) {
		this.arrBytesArchivo = arrBytesArchivo;
	}
	
	
	
}
