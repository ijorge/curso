package com.tarjetamiga.bo;

public class Concepto {
	
	private int id;
	private String descripcion;
	private String importe;
	private SolicitudFactura objSolicitudFactura;
	
	public Concepto() {
		this.id = 0;
		this.descripcion = "";
		this.importe = "";
		this.objSolicitudFactura = new SolicitudFactura();
	}
	
	public Concepto(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public SolicitudFactura getObjSolicitudFactura() {
		return objSolicitudFactura;
	}

	public void setObjSolicitudFactura(SolicitudFactura objSolicitudFactura) {
		this.objSolicitudFactura = objSolicitudFactura;
	}
	
	
	
	

}
