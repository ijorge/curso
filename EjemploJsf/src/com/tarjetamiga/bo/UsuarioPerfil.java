package com.tarjetamiga.bo;

public class UsuarioPerfil {
	
	private Perfil perfil;
	private Usuario usuario;
	
	public UsuarioPerfil() {
		this.perfil = new Perfil();
		this.usuario = new Usuario();
	}
	
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	

}
