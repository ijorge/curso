package com.tarjetamiga.bo;

public class Plaza {

	private Convenio objConvenio;
	private int intId;
	private String strDescripcion;
	
	public Plaza() {
		this.objConvenio = new Convenio();
		this.intId = 0;
		this.strDescripcion = "";
	}
	
	public Convenio getObjConvenio() {
		return objConvenio;
	}
	public void setObjConvenio(Convenio objConvenio) {
		this.objConvenio = objConvenio;
	}
	public int getIntId() {
		return intId;
	}
	public void setIntId(int intId) {
		this.intId = intId;
	}
	public String getStrDescripcion() {
		return strDescripcion;
	}
	public void setStrDescripcion(String strDescripcion) {
		this.strDescripcion = strDescripcion;
	}
	
	
	
}
