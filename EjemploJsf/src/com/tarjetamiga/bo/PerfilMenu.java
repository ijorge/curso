package com.tarjetamiga.bo;

public class PerfilMenu {
	
	private int id;
	private Perfil perfil;
	private Menu menu;
	private boolean agregar;
	private boolean editar;
	private boolean eliminar;
	private boolean consulta;
	private boolean seleccionado;
	
	public PerfilMenu() {
		this.id = 0;
		this.perfil = new Perfil();
		this.menu = new Menu();
		this.agregar = false;
		this.editar = false;
		this.eliminar = false;
		this.consulta = false;
		this.seleccionado = false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public boolean isAgregar() {
		return agregar;
	}
	public void setAgregar(boolean agregar) {
		this.agregar = agregar;
	}
	public boolean isEditar() {
		return editar;
	}
	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	public boolean isEliminar() {
		return eliminar;
	}
	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}
	public boolean isConsulta() {
		return consulta;
	}
	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}
	public boolean isSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	

}
