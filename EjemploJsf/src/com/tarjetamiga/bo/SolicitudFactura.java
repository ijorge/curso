package com.tarjetamiga.bo;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SolicitudFactura {

	private int id;
	private String descripcion;
	private String importeTotal;
	private List<Concepto> lstConceptos;
	private List<Archivo> lstArchivos;
	private Estatus objEstatus;
	private String fechaCreacion;
	private Convenio objConvenio;
	
	public SolicitudFactura() {
		this.id = 0;
		this.descripcion = "";
		this.importeTotal = "";
		this.lstConceptos = new ArrayList<Concepto>();
		this.lstArchivos = new ArrayList<Archivo>();
		this.objEstatus = new Estatus();
		this.fechaCreacion = "";
		this.objConvenio = new Convenio();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}

	public List<Concepto> getLstConceptos() {
		return lstConceptos;
	}

	public void setLstConceptos(List<Concepto> lstConceptos) {
		this.lstConceptos = lstConceptos;
	}

	public List<Archivo> getLstArchivos() {
		return lstArchivos;
	}

	public void setLstArchivos(List<Archivo> lstArchivos) {
		this.lstArchivos = lstArchivos;
	}

	public Estatus getObjEstatus() {
		return objEstatus;
	}

	public void setObjEstatus(Estatus objEstatus) {
		this.objEstatus = objEstatus;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Convenio getObjConvenio() {
		return objConvenio;
	}

	public void setObjConvenio(Convenio objConvenio) {
		this.objConvenio = objConvenio;
	}
	
	
	
}
