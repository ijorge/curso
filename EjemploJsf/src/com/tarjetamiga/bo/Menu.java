package com.tarjetamiga.bo;

public class Menu {
	
	private int id;
	private String descripcion;
	private String url;
	private int padre;
	private String metodo;
	private boolean activo;
		
	
	public Menu() {
		this.id = 0;
		this.descripcion = "";
		this.url = "";
		this.padre = 0;
		this.metodo = "";
		this.activo = false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getPadre() {
		return padre;
	}
	public void setPadre(int padre) {
		this.padre = padre;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	

}
