package com.tarjetamiga.bo;

public class Perfil {
	
	private int id;
	private String descripcion;
	private boolean activo;
	
	public Perfil() {
		this.id = 0;
		this.descripcion = "";
		this.activo = false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	

}
