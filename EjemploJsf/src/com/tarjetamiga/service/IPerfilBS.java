package com.tarjetamiga.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.common.nodo.Node;

public interface IPerfilBS {
	
	/**
	 * Metodo para obtener el menu.
	 * @return 	HashMap, Key es elemento padre. 
	 * 			Object(ArrayList) es la lista de hijos del elemento padre.
	 */
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu();
	
	/**
	 * Obtiene una lista de perfiles en base al tipo de perfil y/o la descripcion, usado para la pantalla de busqueda.
	 * @param descripcion.- Opcional
	 * @param tipoPerfil.- Opcional (0.-Cualquiera, 1.-Es especialista, 2.- No especialista.)
	 * @return lista de perfiles.
	 */
	public ArrayList<Perfil> obtenerListaPerfil(String descripcion);
	
	
	/**
	 * Guarda el detalle del Perfil (Acciones permitidas por modulo.)
	 * @param lstPerfilMenu
	 * @param objPerfil
	 * @return
	 */
	public int guardaPerfil(List<Node<PerfilMenu>> lstPerfilMenu, Perfil objPerfil);
	
	/**
	 * Obtiene el menu, para poder editarlo en la pantalla de modificar/consultar cada perfil.
	 * @param idPerfil, id del perfil que se consultará.
	 * @return
	 */
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenuModificar(int idPerfil);
	
	/**
	 * Metodo para actualizar el encabezado del perfil.
	 * @param objPerfil
	 * @return
	 */
	public Perfil actualizaPerfil(Perfil objPerfil);
	
	/**
	 * Metodo para eliminar el perfil, en base a si id.
	 * @param objPerfil
	 * @return
	 */
	public int eliminarPerfil(Perfil objPerfil);
	
	/**
	 * Valida si un usuario esta asociado a dicho perfil.
	 * @param objPerfil
	 * @return true, si por lo menos un usuario esta asignado al perfil.
	 */
	public boolean verificaPerfilEnUso(Perfil objPerfil);
	
	/**
	 * Verifica si la descripcion del perfil ya existe.
	 * @param objPerfil
	 * @return
	 */
	public boolean verificaDescripcionPerfil(Perfil objPerfil);
	
	/**
	 * Obtiene el perfil de un usuario, en base al id_usuario.
	 * @param objUsuario
	 * @return
	 */
	public Perfil obtenerPerfil(Usuario objUsuario);
	
	/**
	 * Metodo para validar si un usuario ya tiene un perfil asignado.
	 * @param objUsuario
	 * @return true, si el usuario ya esta relacionado a un perfil.
	 */
	public boolean verificaUsuarioConPerfil(Usuario objUsuario);
	
	/**
	 * Metodo para obtener los privilegios de una determinada opcion del menu.
	 * (alta, baja, modificar y eliminar)
	 * @param objPerfil
	 * @param objMenu
	 * @return objeto de tipo PerfilMenu
	 */
	public PerfilMenu obtenerPrivilegiosPorMenu(Perfil objPerfil, Menu objMenu);
	
	/**
	 * Metodo para guardar la lista de opciones del menu que se editaron en la pantalla de modificacion.
	 * @param lstPerfilMenu
	 * @param objPerfil
	 * @return
	 */
	public int guardaPerfilMenu(List<Node<PerfilMenu>> lstPerfilMenu, Perfil objPerfil);

}
