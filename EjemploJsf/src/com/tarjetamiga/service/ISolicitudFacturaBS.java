package com.tarjetamiga.service;

import java.util.Collection;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;

public interface ISolicitudFacturaBS {
	
	public List<SolicitudFactura> obtenerListaSolicitudFactura(SolicitudFactura objSolicitudFactura);
	
	public int crearSolicitud(SolicitudFactura objSolicitudFactura, String correoOrigen, String CorreoDestino,Collection<Archivo> lstArchivo);
	
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura);
	
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura);
	
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura , Collection<Concepto> lstConceptos);
	
	public void insertarArchivo(Collection<Archivo> lstArchivo, SolicitudFactura objSolicitudFactura);

	
}
