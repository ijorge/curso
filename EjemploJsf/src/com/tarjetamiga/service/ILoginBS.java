package com.tarjetamiga.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;

public interface ILoginBS {

	public boolean existeUsuario(Usuario objUsuario);
	
	public boolean validaUsuarioSP(Usuario objUsuario);
	
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu(Usuario objUsuario);
	
}
