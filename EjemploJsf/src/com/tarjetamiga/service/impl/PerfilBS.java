package com.tarjetamiga.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.Perfil;
import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.common.nodo.Node;
import com.tarjetamiga.dao.IPerfilDAO;
import com.tarjetamiga.service.IPerfilBS;

@Service
public class PerfilBS implements IPerfilBS{

	@Autowired
	private IPerfilDAO objPerfilDAO;

	@Override
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu() {
		return objPerfilDAO.obtenerMenu();
	}

	@Override
	public ArrayList<Perfil> obtenerListaPerfil(String descripcion) {
		// TODO Auto-generated method stub
		return  objPerfilDAO.obtenerListaPerfil(descripcion);
	}


	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	@Override
	public int guardaPerfil(List<Node<PerfilMenu>> lstPerfilMenu, Perfil objPerfil) {
		// TODO Auto-generated method stub
		
		Perfil objPerfilLocal = objPerfilDAO.guardaPerfil(objPerfil);//Guarda Perfil y obtiene id_perfil
		
		return objPerfilDAO.guardaPerfilMenu(lstPerfilMenu, objPerfilLocal);
		
	}
	
	public int guardaPerfilMenu(List<Node<PerfilMenu>> lstPerfilMenu, Perfil objPerfil) {
		// TODO Auto-generated method stub
		
		return objPerfilDAO.guardaPerfilMenu(lstPerfilMenu, objPerfil);
		
	}

	@Override
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenuModificar(
			int idPerfil) {
		// TODO Auto-generated method stub
		return objPerfilDAO.obtenerMenuModificar(idPerfil);
	}

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	@Override
	public Perfil actualizaPerfil(Perfil objPerfil) {
		
		//Elimina el detalle del perfil, para despues volverlo a insertar.
		objPerfilDAO.eliminarDetallePerfilMenu(objPerfil);
		
		//Actualiza el encabezado.
		return objPerfilDAO.actualizaPerfil(objPerfil);
	}

	@Override
	public int eliminarPerfil(Perfil objPerfil) {
		// TODO Auto-generated method stub
		return objPerfilDAO.eliminarPerfil(objPerfil);
	}

	@Override
	public boolean verificaPerfilEnUso(Perfil objPerfil) {
		// TODO Auto-generated method stub
		return objPerfilDAO.verificaPerfilEnUso(objPerfil);
	}

	@Override
	public boolean verificaDescripcionPerfil(Perfil objPerfil) {
		// TODO Auto-generated method stub
		return objPerfilDAO.verificaDescripcionPerfil(objPerfil);
	}

	@Override
	public Perfil obtenerPerfil(Usuario objUsuario) {
		// TODO Auto-generated method stub
		return objPerfilDAO.obtenerPerfil(objUsuario);
	}

	@Override
	public boolean verificaUsuarioConPerfil(Usuario objUsuario) {
		// TODO Auto-generated method stub
		return objPerfilDAO.verificaUsuarioConPerfil(objUsuario);
	}

	@Override
	public PerfilMenu obtenerPrivilegiosPorMenu(Perfil objPerfil, Menu objMenu) {
		// TODO Auto-generated method stub
		return objPerfilDAO.obtenerPrivilegiosPorMenu(objPerfil, objMenu);
	}
	
}
