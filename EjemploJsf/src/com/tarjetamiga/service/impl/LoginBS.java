package com.tarjetamiga.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.dao.ILoginDAO;
import com.tarjetamiga.service.ILoginBS;

@Service
public class LoginBS implements ILoginBS{
	
	@Autowired
	private ILoginDAO objLoginDao;

	@Override
	public boolean existeUsuario(Usuario objUsuario) {
		boolean esValido = getObjLoginDao().validaUsuario(objUsuario);
		return esValido;
	}
	
	@Override
	public boolean validaUsuarioSP(Usuario objUsuario) {
		boolean esValido =  getObjLoginDao().validaUsuarioSP(objUsuario);
		return esValido;
	}

	public ILoginDAO getObjLoginDao() {
		return objLoginDao;
	}

	public void setObjLoginDao(ILoginDAO objLoginDao) {
		this.objLoginDao = objLoginDao;
	}

	@Override
	public HashMap<Integer, ArrayList<PerfilMenu>> obtenerMenu(
			Usuario objUsuario) {
		// TODO Auto-generated method stub
		return this.objLoginDao.obtenerMenu(objUsuario);
	}

	

}
