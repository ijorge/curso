package com.tarjetamiga.service.impl;


import java.util.Collection;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.tarjetamiga.bo.Archivo;
import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.bo.SolicitudFactura;
import com.tarjetamiga.dao.ISolicitudFacturaDAO;
import com.tarjetamiga.service.ISolicitudFacturaBS;

@Service("solicitudFacturaBS")
public class SolicitudFacturaBS implements ISolicitudFacturaBS{
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
    private JavaMailSender mailSender;
	
	@Autowired
	@Qualifier("solicitudFacturaDAO")
	private ISolicitudFacturaDAO objSolicitudFacturaDAO;
	
	@Autowired
	private TransactionTemplate transactionTemplate;

	@Override
	public List<SolicitudFactura> obtenerListaSolicitudFactura(
			SolicitudFactura objSolicitudFactura) {
		return objSolicitudFacturaDAO.obtenerListaSolicitudFactura(objSolicitudFactura);
	}

//	@Override
//	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
//	public int crearSolicitud(SolicitudFactura objSolicitudFactura) {
//		
//		//int j = 0;
//
//			objSolicitudFacturaDAO.insertarSolicitud(objSolicitudFactura);
//
//			for(Concepto objConcepto : objSolicitudFactura.getLstConceptos())
//			{
//				objSolicitudFacturaDAO.insertarConcepto(objSolicitudFactura, objConcepto);
//				//j++;
//			}
//
////		if(j == 2)
////		{
////			throw new ExcepcionControlada("Se intento crear solicitud solo con 2 registros.");
////		}else{
////			//Enviamos correo
////			enviarCorreo();
////		}
//		
//		return 0;
//	}
	
	public void enviarCorreo(String correoOrigen, String correoDestino){
		// creates a simple e-mail object
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom("jorge.arrambide84@gmail.com");
        email.setTo("jorge.arrambide@outlook.com");
        email.setSubject("probando envio");
        email.setText("Contenido del mensaje.");
         
        // sends the e-mail
        mailSender.send(email);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int crearSolicitud(final SolicitudFactura objSolicitudFactura, final String correoOrigen, final String CorreoDestino, final Collection<Archivo> lstArchivo){
		
		return transactionTemplate.execute(new TransactionCallback() {
			
		      public Object doInTransaction(TransactionStatus status) {
		    	  
		    	  int codigoError = 0;
		    	  
		    	  try {
		    		  
		    		  	codigoError = objSolicitudFacturaDAO.insertarSolicitud(objSolicitudFactura);
				  		
				    	int cantidadDeConceptos = 0;
				    	
				  		for(Concepto objConcepto : objSolicitudFactura.getLstConceptos())
				  		{
				  			objSolicitudFacturaDAO.insertarConcepto(objSolicitudFactura, objConcepto);
				  			cantidadDeConceptos++;
				  		}
				  		
				  		if(cantidadDeConceptos == 3)
				  		{
				  			status.setRollbackOnly();
				  		}else{
				  			insertarArchivo(lstArchivo, objSolicitudFactura);
				  		}
				  		
				  		enviarCorreo(correoOrigen, CorreoDestino);
				  		
				} catch (Exception e) {
					e.printStackTrace();
					status.setRollbackOnly();
					codigoError = -1;
				}
 
		      return codigoError;
		      }
		    });
	}
	
	
	public void crearSolicitud3(final SolicitudFactura objSolicitudFactura){
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			
		      public void doInTransactionWithoutResult(TransactionStatus status) {
		    	  	  
		    	  try {
		    		  
		    		  	objSolicitudFacturaDAO.insertarSolicitud(objSolicitudFactura);
				  		
				    	int cantidadDeConceptos = 0;
				    	
				  		for(Concepto objConcepto : objSolicitudFactura.getLstConceptos())
				  		{
				  			objSolicitudFacturaDAO.insertarConcepto(objSolicitudFactura, objConcepto);
				  			cantidadDeConceptos++;
				  		}
				  		
				  		if(cantidadDeConceptos == 3)
				  		{
				  			status.setRollbackOnly();
				  		}  
				  		
				} catch (Exception e) {
					e.printStackTrace();
					status.setRollbackOnly();
				}
 
		      }
		    });
	}
	
	
	public void crearSolicitud2(final SolicitudFactura objSolicitudFactura){

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			
		      public void doInTransactionWithoutResult(TransactionStatus status) {

		    	  try {
		    		  	objSolicitudFacturaDAO.insertarSolicitud(objSolicitudFactura);
				  		
				    	int cantidadDeConceptos = 0;
				    	
				  		for(Concepto objConcepto : objSolicitudFactura.getLstConceptos())
				  		{
				  			objSolicitudFacturaDAO.insertarConcepto(objSolicitudFactura, objConcepto);
				  			cantidadDeConceptos++;
				  		}
				  		
				  		if(cantidadDeConceptos == 3)
				  		{
				  			status.setRollbackOnly();
				  		}
		    		  
				} catch (Exception e) {
					e.printStackTrace();
					status.setRollbackOnly();
				}
 
		      }
		    });
	}

	@Override
	public SolicitudFactura obtenerDetalleSolicitudFactura(int idSolcitudFactura) {
		
		SolicitudFactura objSolicitudFactura = objSolicitudFacturaDAO.obtenerDetalleSolicitudFactura(idSolcitudFactura);
		
		return objSolicitudFactura;
	}

	@Override
	public List<Concepto> obtenerDetalleConcepto(int idSolcitudFactura) {
		return objSolicitudFacturaDAO.obtenerDetalleConcepto(idSolcitudFactura);
	}

	@Override
	public void modificarSolicitudFactura(SolicitudFactura objSolicitudFactura, Collection<Concepto> lstConceptos) {
		
		objSolicitudFacturaDAO.modificarSolicitudFactura(objSolicitudFactura);	
		
		objSolicitudFacturaDAO.eliminarConceptos(objSolicitudFactura);
		
		for(Concepto objConcepto : lstConceptos)
		{
			objSolicitudFacturaDAO.insertarConcepto(objSolicitudFactura, objConcepto);
		}
		
	}

	@Override
	public void insertarArchivo(Collection<Archivo> lstArchivo, SolicitudFactura objSolicitudFactura) {
		
		for(Archivo objArchivo : lstArchivo)
		{
			objSolicitudFacturaDAO.insertarArchivo(objArchivo, objSolicitudFactura);
		}
		
	}



}
