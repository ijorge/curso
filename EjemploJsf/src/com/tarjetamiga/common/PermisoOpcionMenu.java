package com.tarjetamiga.common;

public class PermisoOpcionMenu {
	
	private boolean habilitaBotonAgregar;
	private boolean habilitaBotonModificar;
	private boolean habilitaBotonConsultar;
	private boolean habilitaBotonEliminar;
	
	public boolean isHabilitaBotonAgregar() {
		return habilitaBotonAgregar;
	}
	public void setHabilitaBotonAgregar(boolean habilitaBotonAgregar) {
		this.habilitaBotonAgregar = habilitaBotonAgregar;
	}
	public boolean isHabilitaBotonModificar() {
		return habilitaBotonModificar;
	}
	public void setHabilitaBotonModificar(boolean habilitaBotonModificar) {
		this.habilitaBotonModificar = habilitaBotonModificar;
	}
	public boolean isHabilitaBotonConsultar() {
		return habilitaBotonConsultar;
	}
	public void setHabilitaBotonConsultar(boolean habilitaBotonConsultar) {
		this.habilitaBotonConsultar = habilitaBotonConsultar;
	}
	public boolean isHabilitaBotonEliminar() {
		return habilitaBotonEliminar;
	}
	public void setHabilitaBotonEliminar(boolean habilitaBotonEliminar) {
		this.habilitaBotonEliminar = habilitaBotonEliminar;
	}	

}
