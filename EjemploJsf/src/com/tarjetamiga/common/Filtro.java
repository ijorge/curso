package com.tarjetamiga.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tarjetamiga.bo.Usuario;

public class Filtro implements Filter{
	
	//Variables de sesion
	private static final String USUARIO = "usuario";
	private static final String VERIFICA = "peticionPOST" ;
	

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        
        //Para caracteres especiales. (acentos,etc.)
        if (request.getCharacterEncoding() == null) {
            request.setCharacterEncoding("UTF-8");
        }
        
        //Se obtiene de sesion el usuario.
        Usuario usuario = (session != null) ? (Usuario) session.getAttribute(USUARIO) : null;
        
        //URL del login.
        String loginURL = request.getContextPath() + "/login.xhtml";
        String urlErrorPagina = request.getContextPath() + "/errorSession.xhtml";
        String requestURI = request.getRequestURI();
        
        
        if (request.getRequestURI().startsWith(request.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER) == false) { 
        	// Si entra omite los JSF resources (CSS/JS/Images/etc), para que si queden en cache; las paginas no quedan en cache (boton atras de navegador deshabilitado.)
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0); // Proxies.
            
            
            if (usuario == null && !(requestURI.equals("/EjemploJsf/") || request.getRequestURI().equals(loginURL) || request.getRequestURI().equals(urlErrorPagina)) ) {
            	//Si termino la sesion y (la URL NO es la pagina de login ó no es la URL de fin de sesion)
            	 String urlSesion = request.getContextPath() + "/errorSession.xhtml";
            	 if(esPeticionAjax(request) == false){
            		 response.sendRedirect(urlSesion);
            	 }else{
            		 String redirectURL = response.encodeRedirectURL(request.getContextPath() + "/errorSession.xhtml");
            		 StringBuilder sb = new StringBuilder();
            		    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"").append(redirectURL).append("\"></redirect></partial-response>");
            		    response.setHeader("Cache-Control", "no-cache");
            		    response.setCharacterEncoding("UTF-8");
            		    response.setContentType("text/xml");
            		    PrintWriter pw = response.getWriter();
            		    pw.println(sb.toString());
            		    pw.flush();
            	 }
            } else {
            	

            	if(usuario != null)
            	{//Si aun existe sesion
            	
            			
	            		//Se implemento lo siguiente debido a:
	            		//Se esta usando faces-redirect=true, por lo que hay una peticion POST, seguida por un GET
	            		//Se usa faces-redirect=true, ya que algunos componentes de PrimeFaces 4.0, no se renderean (combos, checkbox de datatable) de manera correcta al regresar de una pagina a otra.
	            		
		            	if(request.getMethod().equalsIgnoreCase("POST")){
		            		//Si es peticion POST, se sube a sesion la variable VERIFICA, con el valor "1"
		            		session.setAttribute(VERIFICA, "1");
		            		chain.doFilter(request, response);
		            		
		                }else{
		                		//Entonces es peticion GET
		                		
		                		if(validaURL(request) == true)
		                		{
		                			
			                		//Se obtiene la variable almacenada al haber pasado por POST.
				                	String verificado = session.getAttribute(VERIFICA) != null ? (String)session.getAttribute(VERIFICA) : "";
				                	
				                	if(verificado.equals("1") == false)
				                	{//Si no tiene el valor "1", indica que el usuario introdujo directamente la url en el navegador.
				                		
				                		//Eliminamos la sesion
				                		session.invalidate();
				                		
				                		//Redirigir a home
				                		response.sendRedirect(request.getContextPath());
				                		
				                	}else{
				                		//Si tiene el valor "1", entonces se paso previamente por POST.
				                		
				                		//Removemos el valor para volverlo a subir cuando haya otra peticion POST.
				                		session.removeAttribute(VERIFICA);
				                		
				                		chain.doFilter(request, response);
				                	}
			                	
		                		}else{
		                			chain.doFilter(request, response);
		                		}
		                	
		                }
		           
	            	
            	}
            	else{
            		
            		chain.doFilter(request, response);
            		
            	}

                
            }
            
        }else {
            chain.doFilter(request, response);
        }
        
	}
	
	/**
	 * Metodo para no validar URLs que necesariamente solo hacen peticiones GET
	 * @param request
	 * @return
	 */
	public boolean validaURL(HttpServletRequest request){
		
		boolean pasarURL = false;
		
		if(request.getRequestURI().equals("/EjemploJsf/inicio.xhtml") == false //Url que al dar clic en el logo redirige a la pantalla de inicio (Pantalla de bienvenida)
		//|| request.getRequestURI().equals("/CADCO/ruta de url/archivo.xhtml") == false //Ejemplo de otra URL que no se desea que se valide.
		)
		{
			pasarURL = true;
		}
		
		return pasarURL;
		
	}
	
	private boolean esPeticionAjax(HttpServletRequest request) {
	    boolean check = false;
	    String facesRequest = request.getHeader("Faces-Request");
	    if (facesRequest != null && facesRequest.equals("partial/ajax")) {
	        check = true;
	    }
	    return check;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}

