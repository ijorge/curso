package com.tarjetamiga.common.handlerException;

import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;


public class CustomExceptionHandler extends ExceptionHandlerWrapper{

	private ExceptionHandler wrapped;
	
	static final Logger logger = LogManager.getLogger(CustomExceptionHandler.class.getName());

    public CustomExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void handle() throws FacesException {

    	Iterator<ExceptionQueuedEvent> iterator = getUnhandledExceptionQueuedEvents().iterator();

    	while (iterator.hasNext()) {
            ExceptionQueuedEvent event = iterator.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext)event.getSource();
            Throwable throwable = context.getException();
             

            	
                String error = throwable.getMessage()+"("+throwable.getClass().getName()+")";
                 
                if ( throwable.getClass()==javax.faces.application.ViewExpiredException.class )
                {
                    error="Su sesion expiró. Por favor vuelva a entrar al sistema.";
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sesion expiro", error );               
                    RequestContext.getCurrentInstance().showMessageInDialog(message);
                   
                }
                	
                logger.error("Ocurrio un error.",throwable);
                

          }
    	
          getWrapped().handle();
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

}
