package com.tarjetamiga.common;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpSession;

import com.tarjetamiga.bo.Menu;
import com.tarjetamiga.bo.PerfilMenu;
import com.tarjetamiga.bo.Usuario;
import com.tarjetamiga.service.IPerfilBS;

public abstract class AbstractView{

	protected boolean habilitaBotonAgregar = false;
	protected boolean habilitaBotonConsultar = false;
	protected boolean habilitaBotonModificar = false;
	protected boolean habilitaBotonEliminar = false;
	
	//Variables usadas para la navegacion, almacenadas en scope Flash de JSF
	protected static final String FROM = "DESDE";
	protected static final String VISTA_BUSQUEDA = "BUSQUEDA";
	protected static final String VISTA_ALTA = "ALTA";
	protected static final String VISTA_MODIFICAR = "EDITAR";
	protected static final String VISTA_CONSULTAR_DETALLE = "DETALLE";
	
	protected static final String BOTONES_SESSION = "BOTONES";
	protected static final String USUARIO = "usuario" ;
	
	/**
	 * Metodo para inicializar botones.
	 * @param habilitaBotonAgregar
	 * @param habilitaBotonModificar
	 * @param habilitaBotonConsultar
	 * @param habilitaBotonEliminar
	 */
	public void inicializaBotones(boolean habilitaBotonAgregar,boolean  habilitaBotonModificar, boolean  habilitaBotonConsultar,boolean  habilitaBotonEliminar){
		this.habilitaBotonAgregar = habilitaBotonAgregar;
		this.habilitaBotonModificar = habilitaBotonModificar;
		this.habilitaBotonConsultar = habilitaBotonConsultar;
		this.habilitaBotonEliminar = habilitaBotonEliminar;		
	}
	
	/**
	 * Obtiene el objeto Usuario de sesion
	 * @return
	 */
	public Usuario obtenerUsuario(){
		
		//Inicializacion de variables.
		
		FacesContext fc = FacesContext.getCurrentInstance();
		
		//Se obtiene la sesion
		HttpSession session = (HttpSession)fc.getExternalContext().getSession(false);
		
		Usuario objUsuario = (Usuario) session.getAttribute(USUARIO);
		
		return objUsuario;
		
	}
	
	/**
	 * Metodo que debe de llamarse al inicializar la pagina de consultas.
	 */
	public void habilitaAcciones(){
		
		FacesContext fc = FacesContext.getCurrentInstance();
		
		//Se obtiene la sesion
		HttpSession session = (HttpSession)fc.getExternalContext().getSession(false);
		
		//Se obtiene el objeto ControlButton
		PermisoOpcionMenu objControl = (PermisoOpcionMenu) session.getAttribute(BOTONES_SESSION);
		
		//Si es diferente de nulo
		if(objControl != null)
			inicializaBotones(objControl.isHabilitaBotonAgregar(), objControl.isHabilitaBotonModificar(), objControl.isHabilitaBotonConsultar(), objControl.isHabilitaBotonEliminar());
		
		
	}
	
	/**
	 * Metodo para administrar los mensaje en las pantallas
	 * @param detalle
	 * @param severity
	 */
	public void mensajes(String detalle , Severity severity){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		facesContext.addMessage("mensaje", new FacesMessage(severity, detalle, ""));
	}
	
	/**
	 * Metodo para obtener el mensaje a mostrar en el xhtml (label) o mensajes de error (FacesMessage)
	 * @param llave
	 * @return
	 */
	public String resourceBundleEtiquetas(String llave){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
		
		return bundle.getString(llave);
	}
	
	/**
	 * Metodo para obtener los valores del archivo configuracion.properties.
	 * Usado para concentrar valores
	 * @param llave
	 * @return
	 */
	public String resourceBundlePropiedades(String llave){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "varConfiguracion");
		
		return bundle.getString(llave);
	}
	
	/**
	 * Metodo que pone en sesion los privilegios de las acciones de una opcion del menu.
	 * @param perfilBS
	 * @param objMenu
	 * @param objUsuario
	 */
	public void privilegiosPorMenu(IPerfilBS perfilBS, Menu objMenu, Usuario objUsuario){
		
		PerfilMenu objPerfilMenu = new PerfilMenu();
		
		objPerfilMenu = perfilBS.obtenerPrivilegiosPorMenu(objUsuario.getObjPerfil(), objMenu);
		
		PermisoOpcionMenu objControl = new PermisoOpcionMenu();
		 
		objControl.setHabilitaBotonAgregar(objPerfilMenu.isAgregar());
		objControl.setHabilitaBotonConsultar(objPerfilMenu.isConsulta());
		objControl.setHabilitaBotonModificar(objPerfilMenu.isEditar());
		objControl.setHabilitaBotonEliminar(objPerfilMenu.isEliminar());
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		session.setAttribute(BOTONES_SESSION, objControl);
	}
	
	public void subir(String variable, Object obj){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		session.setAttribute(variable, obj);
	}
	
	/**
	 * Obtiene el objeto flash del contexto de JSF.
	 * @return
	 */
	public Flash obtenerFlash(){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		Flash flash = facesContext.getExternalContext().getFlash();
		flash.setKeepMessages(true);
		flash.setRedirect(true);
		
		return flash;
	}
	
	
	public void setHabilitaBotonAgregar(boolean habilitaBotonAgregar) {
		this.habilitaBotonAgregar = habilitaBotonAgregar;
	}

	public boolean isHabilitaBotonAgregar() {
		return habilitaBotonAgregar;
	}

	public void setHabilitaBotonConsultar(boolean habilitaBotonConsultar) {
		this.habilitaBotonConsultar = habilitaBotonConsultar;
	}

	public boolean isHabilitaBotonConsultar() {
		return habilitaBotonConsultar;
	}

	public void setHabilitaBotonModificar(boolean habilitaBotonModificar) {
		this.habilitaBotonModificar = habilitaBotonModificar;
	}

	public boolean isHabilitaBotonModificar() {
		return habilitaBotonModificar;
	}

	public void setHabilitaBotonEliminar(boolean habilitaBotonEliminar) {
		this.habilitaBotonEliminar = habilitaBotonEliminar;
	}

	public boolean isHabilitaBotonEliminar() {
		return habilitaBotonEliminar;
	}
	
	
	
}
