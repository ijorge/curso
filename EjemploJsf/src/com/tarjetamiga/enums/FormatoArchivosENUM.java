package com.tarjetamiga.enums;

import java.util.HashMap;
import java.util.Map;

public enum FormatoArchivosENUM {
	/**
	 * Representa formatos utilizados
	 */	

	PDF(1,"Pdf","application/pdf"),
	XML(2,"Xml","text/xml"),
	XLS(3,"Xls","application/vnd.ms-excel"),
	XLSX(4,"Xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
	DOC(5,"Doc","application/msword"),
	DOCX(6,"Docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
	PPT(7,"Ppt","application/vnd.ms-powerpoint"),
	PPTX(8,"Pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"),
	HTML(9,"Html","text/html"),
	HTM(10,"Htm","text/html"),
	JPG(11,"Jpg","image/jpeg"),
	JPEG(12,"Jpeg","image/jpeg"),
	ZIP(13,"Zip","application/zip"),
	TXT(14,"Txt","text/plain"),
	PNG(15,"Png","image/png")
	
	;
	
	    

	     /**
	      * Determina el codigo asociado 
	      */
	     private int codigo;
	     /**
	      * Provee la descripcion
	      */
	     private String descripcion;
	     
	     /**
	      * Tipo de contenido
	      */
	     private String tipoContenido;
	     
	    /**
	     * Constructor de las literales del Enum
	     * 
	     * @param argCodigo
	     * @param argDesc
	     */
	    private FormatoArchivosENUM(int argCodigo, String argDesc, String strTipoContenido){
	            codigo = argCodigo;
	            descripcion = argDesc;
	            tipoContenido = strTipoContenido;
	    }

	    /**
	     * Provee el valor asociado al c�digo que se le env�a
	     * 
	     * @param argCode
	     */
	    public static FormatoArchivosENUM getTipo(int argCode){
	    
	    	FormatoArchivosENUM tipo = null;
	    
	            switch(argCode){
	    
	                case 1: tipo = FormatoArchivosENUM.PDF; break;
	                case 2: tipo = FormatoArchivosENUM.XML; break;
	                case 3: tipo = FormatoArchivosENUM.XLS; break;  
	                case 4: tipo = FormatoArchivosENUM.XLSX; break;  
	                case 5: tipo = FormatoArchivosENUM.DOC; break;
	                case 6: tipo = FormatoArchivosENUM.DOCX; break;
	                case 7: tipo = FormatoArchivosENUM.PPT; break;
	                case 8: tipo = FormatoArchivosENUM.PPTX; break;
	                case 9: tipo = FormatoArchivosENUM.HTML; break;
	                case 10: tipo = FormatoArchivosENUM.HTM; break;
	                case 11: tipo = FormatoArchivosENUM.JPG; break;
	                case 12: tipo = FormatoArchivosENUM.JPEG; break;
	                case 13: tipo = FormatoArchivosENUM.ZIP; break;
	                case 14: tipo = FormatoArchivosENUM.TXT; break;
	            }    
	            return tipo;
	    }
	    
	    
	    private static Map<String, FormatoArchivosENUM> mapTipoContenido = new HashMap<String, FormatoArchivosENUM>();

	    
	    static {
	        for (FormatoArchivosENUM enumTipoServicio : FormatoArchivosENUM.values()) {
	            mapTipoContenido.put(enumTipoServicio.tipoContenido, enumTipoServicio);
	        }
	    }

	    /**
	     * Determina el c�digo asociado
	     */
	    public int getCodigo(){
	            return codigo;
	    }

	    /**
	     * Provee la descripci�n
	     */
	    public String getDescripcion(){
	            return descripcion;
	    }

		public String getTipoContenido() {
			return tipoContenido;
		}
		
		/**
		 * Obtiene el formato (PDF,XML) en base al tipo de contenido ('application/pdf',etc)
		 * @param valorTipoArchivo
		 * @return
		 */
		public static FormatoArchivosENUM getTipoArchivo(String valorTipoArchivo) {
	        return mapTipoContenido.get(valorTipoArchivo);
	    }
	}
