package com.tarjetamiga.webservice;

import java.util.List;

import javax.jws.WebService;

import com.tarjetamiga.bo.Concepto;

@WebService
public interface IFacturaWS {

	
	public List<Concepto> obtenerDetalleConcepto(int idSolicitudFactura);
	
	public int sumar(int parametro1, int parametro2);

	
}
