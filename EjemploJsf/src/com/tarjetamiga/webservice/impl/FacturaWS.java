package com.tarjetamiga.webservice.impl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarjetamiga.bo.Concepto;
import com.tarjetamiga.service.ISolicitudFacturaBS;
import com.tarjetamiga.webservice.IFacturaWS;

@Service
@WebService(endpointInterface = "com.tarjetamiga.webservice.IFacturaWS")
public class FacturaWS implements IFacturaWS{
	
	@Autowired
	private ISolicitudFacturaBS solicitudFacturaBS;

	@WebMethod
	@Override
	public List<Concepto> obtenerDetalleConcepto(int idSolicitudFactura) {
		// TODO Auto-generated method stub
		return solicitudFacturaBS.obtenerDetalleConcepto(idSolicitudFactura);
	}

	@WebMethod
	@Override
	public int sumar(int parametro1, int parametro2) {
		// TODO Auto-generated method stub
		return parametro1 + parametro2;
	}

}
